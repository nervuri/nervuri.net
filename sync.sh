#!/bin/sh

# Sync Gemini capsule and Gopher hole

set -o errexit  # (-e) exit immediately if any command has a non-zero exit status
set -o nounset  # (-u) don't accept undefined variables

# Go where this script is
cd "$(dirname "$0")" || exit

sync() {
  dir=$1
  #netsigil --sign "$dir"
  rsync -rl --delete --info=progress2 "$dir/" "rawtext:public_$dir/"
  ssh rawtext "find public_$dir -type d -exec chmod 755 {} \; && find public_$dir -type f -exec chmod 644 {} \;"
}

sync gemini
sync gopher
