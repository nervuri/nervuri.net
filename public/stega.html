<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="icon" type="image/ico" href="favicon.ico"/>
<meta name="theme-color" content="#000"/>
<meta name="color-scheme" content="dark"/>
<meta name="referrer" content="no-referrer"/>
<title>Tracking via pasted text | nervuri</title>
<meta name="description" content="Plain text steganography and how it can be used against you"/>
<link rel="canonical" href="https://nervuri.net/stega"/>

<link rel="alternate" type="application/atom+xml" title="nervuri Atom feed" href="feed.atom"/>

<link rel="pgpkey authn" href="keys/nervuri-pgp.asc"/>
<link rel="webmention" href="https://webmention.io/nervuri.net/webmention"/>
<link rel="pingback" href="https://webmention.io/nervuri.net/xmlrpc"/>

<link rel="stylesheet" href="style.css"/>
</head>
<body>

<nav class="top">
	<a href=".">Home</a> <span class="hidden">|</span>
	<span class="right">
		<a href="feed.atom" title="subscribe">RSS</a>
	</span>
	<hr/>
</nav>

<main>
<article>
<div class="center">
	<img src="img/backstab.gif" alt="backstab" title="betrayed by technology" width="309" height="300"/>
	<h1>Tracking via pasted text</h1>
	<i>Plain text steganography and how it can be used against you</i>
</div>
<p><a href="https://en.wikipedia.org/wiki/Zero-width_space">Zero-width characters</a> can be used to embed hidden information inside of plain text.  This is of primary concern to journalists and their sources, but it can affect anyone browsing the Internet.  For example, a page can be dynamically generated server-side to include, between every few words:</p>
<ul>
<li>your username, if logged in</li>
<li>your IP address</li>
<li>the current timestamp</li>
</ul>
<p>By copying text from the page and pasting it somewhere public, you would be revealing this information to anyone who knew how to look for it.  Details and demo in this article:</p>
<p><a href="https://medium.com/@umpox/be-careful-what-you-copy-invisibly-inserting-usernames-into-text-with-zero-width-characters-18b4e6f17b66">Be careful what you copy: Invisibly inserting usernames into text with Zero-Width Characters (Tim Ross, 2018)</a></p>
<p>To check if your browser displays zero-width characters, see the <a href="zero-width">zero-width character test</a>.</p>
<p>Other plain text watermarking techniques / canary traps are explained on Zach Aysan's blog:</p>
<ul>
<li><a href="https://www.zachaysan.com/writing/2017-12-30-zero-width-characters">Zero-Width Characters: Invisibly fingerprinting text (2017)</a></li>
<li><a href="https://www.zachaysan.com/writing/2018-01-01-fingerprinting-update">Text Fingerprinting Update: Stories and ideas from readers (2018)</a></li>
</ul>
<p>To fingerprint text, server software could embed a hidden number between every few words, matching a log entry that contains information about the visitor (username, IP address, cookie, browser details, referrer link, timestamp).  For easily finding pasted excerpts online, the software could similarly hide a static page-specific identifier within the text, that can later be put into search engines.</p>
<p>To achieve this, aside from zero-width characters, the software could use some of the other techniques described by Zach Aysan: <em>"differences in dashes (en, em, and hyphens), quotes (straight vs curly), word spelling (color vs colour), and the number of spaces after sentence endings"</em>, different <a href="https://www.jkorpela.fi/chars/spaces.html">types of spaces</a>, <a href="https://en.wikipedia.org/wiki/Homoglyph">homoglyphs</a> (a vs а), diacritic forms (ț vs ţ), ligatures (ﬁ vs fi, Ⅳ vs IV, ½ vs 1/2), as well as inserting hard to detect typos into the text.  However, zero-with characters are by far the most potent technique, since they can be used to encode any number of bits between any two visible characters.</p>
<h2 id="solutions"><a class="toclink" href="#solutions">Solutions</a></h2>
<p>A partial solution is to convert the text to <a href="https://en.wikipedia.org/wiki/ASCII">ASCII</a>, if language allows.  There are also tools such as:</p>
<ul>
<li><a href="https://github.com/sharkdp/bat">bat</a> - displays all zero-width characters when used with the "-A" option.</li>
<li><a href="https://en.wikipedia.org/wiki/Cat_(Unix)">cat</a> - displays all zero-width characters when used with the "-v" option.</li>
<li><a href="https://www.greenwoodsoftware.com/less/">Less (CLI)</a> - displays <em>most</em> zero-width characters when used with the "-U" option.</li>
<li><a href="https://github.com/DavidJacobson/SafeText">SafeText (CLI)</a> - also detects some homoglyphs.  It started out well, but development has stopped; in its current state, there are many problematic characters that it does not detect - see <a href="https://github.com/DavidJacobson/SafeText/issues">issues</a>.</li>
<li>Several browser extensions that detect <strong>a few</strong> zero-width characters.</li>
</ul>
<p>However, they don't protect against the more sophisticated versions of this hack.  A more complete tool would have to include not just a list of forbidden/allowed characters, but also a a spellchecker and a way to detect trailing whitespace - an x-ray mode that might be triggered when dubious text is detected in the clipboard.  And not just text, image-based steganography can be used in a similar way.  A technical solution might never be perfect, but it could cover the vast majority of cases.</p>
<p>An almost perfect non-technical solution is to retype the text.  You can also try downloading the page twice from different accounts / IP addresses and <a href="https://en.wikipedia.org/wiki/Diff">diff</a> the two versions, or check if the hashes match.  Another solution is to take a screenshot of the text and run it through <a href="https://en.wikipedia.org/wiki/Optical_character_recognition">OCR</a> software.</p>
<h2 id="tools-for-text-steganography"><a class="toclink" href="#tools-for-text-steganography">Tools for text steganography</a></h2>
<ul>
<li><a href="https://github.com/KuroLabs/stegcloak">StegCloak</a></li>
<li><a href="https://www.spammimic.com/">Spam Mimic</a> (see Encode -&gt; Alternate encodings)</li>
<li><a href="https://github.com/vedhavyas/zwfp">zwfp</a></li>
<li><a href="http://www.darkside.com.au/snow/">SNOW</a></li>
<li><a href="https://voidnet.tech/snow10/">Snow10</a></li>
<li><a href="https://web.archive.org/web/20180217185500/http://mok-kong-shen.de:80/">WORDLISTTEXTSTEGANOGRAPHY &amp; EMAILSTEGANO</a></li>
<li><a href="https://git.planetrenox.com/inzerosight/browser-extension">inØsight — Zero Width Obfuscation</a> (extension for Firefox and Chromium)</li>
<li><a href="https://zws.im/">Zero Width Shortener</a> - Shorten URLs using invisible spaces</li>
</ul>
<p><a href="https://www.fileformat.info/info/unicode/char/search.htm">Unicode character search</a></p>
<h2 id="further-reading"><a class="toclink" href="#further-reading">Further reading</a></h2>
<h3 id="text-steganography"><a class="toclink" href="#text-steganography">Text steganography</a></h3>
<ul>
<li><a href="https://www.researchgate.net/publication/321844767_Text_based_steganography">Text based steganography (Robert Lockwood and Kevin Curran, 2017)</a></li>
<li><a href="https://www.ijcsi.org/papers/IJCSI-9-4-3-401-405.pdf">Text Steganography with Multi level Shielding (Sharon Rose Govada et al., 2012) [PDF]</a></li>
<li><a href="https://crypto.stackexchange.com/questions/6058/any-efficient-text-based-steganographic-schemes">Any efficient text-based steganographic schemes? (crypto.stackexchange.com)</a></li>
<li><a href="https://security.stackexchange.com/questions/20414/steganography-to-hide-text-within-text">Steganography to hide text within text (security.stackexchange.com)</a></li>
<li><a href="https://en.wikipedia.org/wiki/Chaffing_and_winnowing">Chaffing and winnowing (Wikipedia)</a></li>
</ul>
<h3 id="control-characters"><a class="toclink" href="#control-characters">Control characters</a></h3>
<ul>
<li><a href="https://en.wikipedia.org/wiki/Zero-width_space">Zero-width space (Wikipedia)</a></li>
<li><a href="https://www.ptiglobal.com/2018/04/26/the-beauty-of-unicode-zero-width-characters/">Article explaining the role of a few zero-width characters</a></li>
<li><a href="https://www.jkorpela.fi/chars/spaces.html">Partial list of Unicode spaces</a></li>
<li><a href="https://en.wikipedia.org/wiki/Unicode_control_characters">Unicode control characters (Wikipedia)</a></li>
<li><a href="https://en.wikipedia.org/wiki/Tags_(Unicode_block)">Tags (Unicode block) (Wikipedia)</a></li>
<li><a href="https://www.unicode.org/Public/UCD/latest/">Unicode Character Database</a></li>
<li><a href="https://dgl.cx/2023/09/ansi-terminal-security">ANSI Terminal security in 2023 and finding 10 CVEs</a></li>
</ul>
<h3 id="homoglyphs"><a class="toclink" href="#homoglyphs">Homoglyphs</a></h3>
<ul>
<li><a href="https://en.wikipedia.org/wiki/Homoglyph">Homoglyph (Wikipedia)</a></li>
<li><a href="https://util.unicode.org/UnicodeJsps/confusables.jsp">Unicode Utilities: Confusables</a></li>
<li><a href="https://www.unicode.org/reports/tr39/#Confusable_Detection">Confusable detection</a></li>
<li><a href="https://unicode.org/Public/security/latest/confusables.txt">confusables.txt</a></li>
</ul>
<h3 id="nfkc-normalisation"><a class="toclink" href="#nfkc-normalisation">NFKC normalisation</a></h3>
<ul>
<li><a href="https://github.com/DavidJacobson/SafeText/issues/1">"Apply NFKC normalisation" - SafeText issue</a></li>
<li><a href="https://www.unicode.org/faq/normalization.html">Unicode Normalization FAQ</a></li>
<li><a href="https://unicode.org/reports/tr15/">Unicode Normalization Forms</a></li>
</ul>
<h3 id="unicode-security-considerations"><a class="toclink" href="#unicode-security-considerations">Unicode security considerations</a></h3>
<ul>
<li><a href="https://www.unicode.org/faq/security.html">Unicode Security Issues FAQ</a></li>
<li><a href="https://www.unicode.org/reports/tr36/">Unicode Security Considerations - Technical Report</a></li>
</ul>
</article>
</main>

<hr/>
<footer class="bottom">
Published: <time datetime="2021-02-20">2021-02-20</time><br/>
Updated: <time datetime="2023-03-28">2023-03-28</time><br/>
<a href="https://gitlab.com/nervuri/nervuri.net/-/blob/master/markdown/stega.md">Source</a> (contributions welcome)<br/>
License: <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
</footer>

</body>
</html>
