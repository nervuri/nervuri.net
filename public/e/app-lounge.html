<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="icon" type="image/ico" href="../favicon.ico"/>
<meta name="theme-color" content="#000"/>
<meta name="color-scheme" content="dark"/>
<meta name="referrer" content="no-referrer"/>
<title>Analysis of the /e/OS app installer, part 2 | nervuri</title>
<meta name="description" content="Issues with App Lounge, the new /e/OS app installer"/>
<link rel="canonical" href="https://nervuri.net/e/app-lounge"/>

<link rel="alternate" type="application/atom+xml" title="nervuri Atom feed" href="../feed.atom"/>

<link rel="pgpkey authn" href="../keys/nervuri-pgp.asc"/>
<link rel="webmention" href="https://webmention.io/nervuri.net/webmention"/>
<link rel="pingback" href="https://webmention.io/nervuri.net/xmlrpc"/>

<link rel="stylesheet" href="../style.css"/>
</head>
<body>

<nav class="top">
	<a href="../.">Home</a> <span class="hidden">|</span>
	<span class="right">
		<a href="../feed.atom" title="subscribe">RSS</a>
	</span>
	<hr/>
</nav>

<main>
<article>
<h1>Analysis of the /e/OS app installer, part 2</h1>
<hr />
<div class="toc">
<ul>
<li><a href="#signature-verification">Signature verification</a></li>
<li><a href="#progressive-web-apps">Progressive web apps</a></li>
<li><a href="#privacy">Privacy</a></li>
<li><a href="#promoting-spyware">Promoting spyware</a></li>
<li><a href="#other-potential-problems-untested">Other potential problems (untested)</a></li>
<li><a href="#final-thoughts">Final thoughts</a></li>
<li><a href="#discussion">Discussion</a></li>
</ul>
</div>
<hr />
<p>In <a href="apps">the first part</a> of this analysis (October 2021), I detailed a number of security and privacy problems with the <a href="https://e.foundation/">/e/OS</a> app installer.  To recap:</p>
<blockquote>
<p><a href="https://gitlab.e.foundation/e/apps/apps">"Apps"</a>, the /e/ app installer, downloads applications from <a href="https://info.cleanapk.org/">CleanAPK.org</a>, an intermediary which provides apps that originate from <a href="https://f-droid.org/">F-Droid</a> and <a href="apps#app-sources">elsewhere</a>.</p>
<p>Since apps are not downloaded directly from F-Droid or Google Play, the installer takes certain <a href="https://info.cleanapk.org/">measures</a> to protect against tampering.  Unfortunately, these measures can be bypassed in the majority of cases.  This means that CleanAPK.org (or whoever compromises it) can get maliciously modified apps installed on /e/ users' devices, either when the user is installing a new app or during the update process.</p>
<p>Such an attack can be targeted at specific users, based on device information which the installer reveals to the CleanAPK server every time it checks for updates, namely: the list of installed apps, device model, build ID, Android version and installed languages.</p>
</blockquote>
<p>Since then, things have changed somewhat.  May 2022 brought the launch of /e/OS v1.0, as well as <a href="https://community.e.foundation/t/app-lounge-know-all-about-it/42331">App Lounge</a>, a rewrite of the installer which downloads applications directly from Google Play.</p>
<h2 id="signature-verification"><a class="toclink" href="#signature-verification">Signature verification</a></h2>
<p>I expected this release to solve all security vulnerabilities detailed in the previous article, but it wasn't until <a href="https://gitlab.e.foundation/e/os/releases/-/releases#please-welcome-eos-13-rocket">/e/OS v1.3</a> (App Lounge v2.3.4) that <a href="https://gitlab.e.foundation/e/os/apps/-/merge_requests/177">they got dealt with</a>.  The first few versions of App Lounge had F-Droid signature verification removed, which made the situation worse than before.  CleanAPK could have gotten the user to install a malicious version of <em>any</em> app just by including it in an API response (and still can, for those who haven't updated).</p>
<p>While understated by the developers, solving this problem was likely the most important security enhancement that the app installer ever received, so be sure to update to /e/OS 1.3 or later (App Lounge currently does not have a self-update mechanism; it is only updated along with the entire operating system).</p>
<p>So progress has been made.  I have not tested the patch, but it looks like, four years after the initial release, the app installer finally provides adequate protection against application spoofing.  This covers native apps, which are in the majority.  PWAs, however, are a separate discussion.</p>
<h2 id="progressive-web-apps"><a class="toclink" href="#progressive-web-apps">Progressive web apps</a></h2>
<p>Alongside native apps from Google Play and F-Droid, the app installer also provides <a href="https://en.wikipedia.org/wiki/Progressive_web_app">progressive web apps (PWAs)</a>: web pages which can be installed alongside native applications.  CleanAPK provides <a href="https://api.cleanapk.org/v2/apps?action=list_apps&amp;type=pwa">a list of links</a> to PWAs from all over the web.  When you install a PWA, App Lounge does nothing to verify that the link pointing to it is correct.  CleanAPK can easily change any of these links and serve spoofed PWAs.</p>
<p>Responding to this observation, one of the developers mentioned that /e/ will be looking into <a href="https://www.w3.org/TR/miniapp-packaging/#sec-miniapp-processing">W3C MiniApps</a>, which can be packaged and verified.  I don't know what their plans for MiniApps are, but as long as App Lounge provides PWAs, something ought to be done to secure their delivery.</p>
<p>My suggestion to /e/ devs is to make App Lounge update its PWA index by fetching a verified, offline-signed list from an /e/ server.  Note, however, that this would only protect the list of links.  It would not help against PWA servers themselves being compromised, which is something that web apps in general are not protected against.  <a href="https://github.com/tasn/webext-signed-pages">Web apps <em>can</em> be signed</a>, but hardly ever are, because browsers have no built-in way to verify such signatures.</p>
<p>I also suggest more prominently presenting the PWA's URL within App Lounge, so that users have a better chance to spot at least obvious alterations, such as "uber.com" being changed to "evil.xxx".  App Lounge currently displays each PWA's domain name under the "package name" field at the bottom of the app details page, which is easy to miss.</p>
<p>A quirk of CleanAPK is that all PWA links that it provides end with the redundant parameters <code>?force=pwa&amp;source=mlpwa</code>.  The <code>source</code> parameter looks like it may be a referrer signal, but a developer told me that this is not intended nor required and asked me to open a ticket for it.  Here is that ticket: <a href="https://gitlab.e.foundation/e/backlog/-/issues/5767">https://gitlab.e.foundation/e/backlog/-/issues/5767</a>.  I opened it three months ago, no change yet.</p>
<p>Side note: around March 2022, /e/ devs <a href="https://community.e.foundation/t/week-13-2022-development-and-testing-updates/39929#app-lounge-9">were planning</a> to get PWAs from <a href="https://www.pwastore.com/">pwastore.com</a> rather than CleanAPK, but they seem to have dropped that idea.</p>
<h2 id="privacy"><a class="toclink" href="#privacy">Privacy</a></h2>
<p>App Lounge inherits the previous installer's <a href="apps#privacy-issues">privacy problems</a> and adds a few more.  Namely, <a href="https://doc.e.foundation/calls_to_google_servers#app-lounge">it sends Google</a> the list of apps you have installed and the following device properties:</p>
<ul>
<li><code>Build.BOOTLOADER</code></li>
<li><code>Build.BRAND</code></li>
<li><code>Build.DEVICE</code></li>
<li><code>Build.FINGERPRINT</code></li>
<li><code>Build.HARDWARE</code></li>
<li><code>Build.ID</code></li>
<li><code>Build.MANUFACTURER</code></li>
<li><code>Build.MODEL</code></li>
<li><code>Build.PRODUCT</code></li>
<li><code>Build.RADIO</code></li>
<li><code>Build.VERSION.RELEASE</code></li>
<li><code>Build.VERSION.SDK_INT</code></li>
<li><code>Features</code></li>
<li><code>GL.Extensions</code></li>
<li><code>GL.Version</code></li>
<li><code>GSF.version</code></li>
<li><code>HasFiveWayNavigation</code></li>
<li><code>HasHardKeyboard</code></li>
<li><code>Keyboard</code></li>
<li><code>Locales</code></li>
<li><code>Navigation</code></li>
<li><code>Platforms</code></li>
<li><code>Screen.Density</code></li>
<li><code>Screen.Height</code></li>
<li><code>Screen.Width</code></li>
<li><code>ScreenLayout</code></li>
<li><code>SharedLibraries</code></li>
<li><code>TouchScreen</code></li>
<li><code>UserReadableName</code></li>
<li><code>Vending.versionString</code></li>
<li><code>Vending.version</code></li>
</ul>
<p>This info is sent regardless of whether the user logs in with a Google account.  An example of the values these properties can take is available <a href="https://gitlab.e.foundation/e/documentation/user/-/merge_requests/651#note_218593">here</a>.  The corresponding source code can be found <a href="https://gitlab.e.foundation/e/os/apps/-/blob/89a9d4b41136a5c3d3fd400b1554ab2ea5e9fd18/app/src/main/java/foundation/e/apps/api/gplay/utils/NativeDeviceInfoProviderModule.kt#L42">here</a>.</p>
<p>It appears that sending this data is at least partly necessary in order to get apps from Google.  Aurora Store behaves similarly, although <a href="https://gitlab.com/AuroraOSS/AuroraStore/-/blob/master/POLICY.md">its privacy policy</a> does not go into much detail (I opened <a href="https://gitlab.com/AuroraOSS/AuroraStore/-/issues/841">an issue</a> about this).  However, unlike App Lounge, Aurora Store provides <a href="https://web.archive.org/web/20220111161806/https://auroraoss.com/faq/#spoofing">a way to spoof</a> device information it sends to Google.</p>
<p>I want to stress that the only way to get apps from Google Play without having to trust a third party is to download them straight from Google, which implies disclosing (at least some of) this data.  I don't fault /e/ for using this method to gain access to Google Play apps.  However, the user should be made aware of its privacy implications and be allowed to opt out before any connection is made to Google.</p>
<p><strong>UPDATE 2023-02-15: <a href="https://e.foundation/leaving-apple-google-welcome-e-os-1-8-do-not-miss-a-special-offer-for-murena-fairphone-4/">Starting with of /e/OS 1.8</a>, App Lounge has a "no-Google mode", allowing users to opt out.</strong></p>
<p>As for HTTP headers, the User-Agent header <a href="https://gitlab.e.foundation/search?group_id=9&amp;project_id=355&amp;scope=merge_requests&amp;search=agent">is no longer as revealing</a> as it was, but this doesn't help against Google, because the device details listed above are a <em>large</em> superset of the info that used to be exposed in the User-Agent string.  The list of languages installed on your system is still exposed via the Accept-Language header, which is apparently required in order to get applications in the correct language.</p>
<p>Data sent to Google is only documented in <a href="https://doc.e.foundation/calls_to_google_servers#app-lounge">an obscure page</a> on the e.foundation website, which is better than nothing – in today's world it even stands out as going above and beyond.  But privacy-related information should be presented clearly to all users before any app sends any data to any server.  Does App Lounge contain an intro screen telling people what data goes where?  No.  The intro screen contains <a href="https://gitlab.e.foundation/e/os/apps/-/blob/1d3e42849149fa2aba49c491a6b2e1433455d9cf/app/src/main/res/raw/terms_of_use.html">a block of legalese</a> that does not mention any of this.</p>
<p>In conclusion, App Lounge sends Google more than enough information to identify most devices, even in its so-called "anonymous mode".  /e/developers should look into spoofing as much of this info as possible.  Regarding the list of installed applications, one way to hide it from Google might be for App Lounge to imitate <a href="https://github.com/EtiennePerot/parcimonie.sh">Parcimonie</a> and do update checks for each app separately, through a proxy (Tor, if possible), at random intervals.</p>
<h2 id="promoting-spyware"><a class="toclink" href="#promoting-spyware">Promoting spyware</a></h2>
<p>When opening App Lounge, the user is greeted with a list of mostly proprietary applications with attractive images, but full of trackers.  App Lounge fetches this list from Google Play.  Allowing Google to determine what gets promoted in /e/'s <em>privacy-focused</em> app store is obviously problematic, as it steers users in the wrong direction.  A privacy-focused system encouraging users to give up their privacy.</p>
<p>The old app installer has pretty much the same problem, except that it gets its app list from CleanAPK, which makes different recommendations based on unknown criteria.</p>
<p>In October 2021, I wrote <a href="https://community.e.foundation/t/an-analysis-of-the-e-os-app-installer/35802/13">a forum post</a> and <a href="https://gitlab.e.foundation/e/backlog/-/issues/4143">a ticket</a> suggesting that the app installer's homepage ought to contain entirely privacy-respecting <a href="https://www.gnu.org/philosophy/free-sw.en.html">free/libre software</a>.  Users could still install whatever they wanted, but they would have to look for the junk, not have it pushed on them.</p>
<p>The issue has been largely ignored until July 2022, when I prodded the devs about it over e-mail, which sparked a semi-private discussion <a href="https://gitlab.e.foundation/e/backlog/-/issues/4143">on the ticket thread</a> (I can only see it while logged in).  In short, they think not having popular crap like Facebook one tap away is bad UX, too inconvenient for most people.  So they intend to leave these apps on the homepage, but are considering a few approaches to improve the situation, such as:</p>
<ul>
<li>excluding apps with a privacy score below 6 (if that doesn't rule out too many popular apps)</li>
<li>highlighting the privacy score</li>
<li>refining the privacy score</li>
<li>modifying the homepage to also promote libre apps</li>
<li>adding a new app category containing privacy-respecting apps recommended by /e/</li>
<li>promoting libre, privacy-friendly alternatives in the app details page</li>
</ul>
<p>Their approach could end up being better than what I initially suggested.  Having mainstream apps on the front page can be an opportunity to educate users: App Lounge could strongly highlight the problematic apps as threats to privacy and discourage their use, promoting good alternatives where available.  Will /e/ devs do this?  It remains to be seen.</p>
<p>The privacy score is already a step in this direction, but I would argue it is insufficient.  It is also objectively unreliable.  To give an example I used previously, Tor Browser has a lower privacy score than Facebook Lite, even though Tor Browser <a href="https://blog.torproject.org/friends-of-tor-match-2020/#comment-290239">does not do any tracking</a>.  Another example: if an app only requests access to contacts and to the internet, it will get a very good privacy score even if it all it does is send your contact list to a data broker.  An automatically-generated privacy score cannot be expected to be accurate in all cases and applying human review to Google Play's entire app catalog is out of the question for /e/.  Still, they could perhaps apply human review to only the most popular apps.  They could also make use of <a href="https://f-droid.org/docs/Anti-Features/#Tracking">F-Droid's tracking flag</a>, which <a href="https://f-droid.org/en/2020/01/16/tracking-the-trackers.html">is arrived at by humans aided by various tools</a>.</p>
<p>The /e/ team posted their current method for calculating the privacy score <a href="https://community.e.foundation/t/app-lounge-know-all-about-it/42331#be-informed-regarding-privacy-4">in a recent developer blog post</a>.  There they <a href="https://community.e.foundation/t/app-lounge-know-all-about-it/42331#improved-privacy-score-10">acknowledge its limitations</a> and claim that they are looking into ways to improve its accuracy.</p>
<p>Aude M. <a href="https://gitlab.e.foundation/e/backlog/-/issues/4143#note_241481">mentioned</a> requesting a study to determine the best solution for the homepage problem, but <a href="https://gitlab.e.foundation/e/backlog/-/issues/5776">the link to the study</a> is dead and no information has been provided on what it entails.  All in all, I am disappointed by the lack of transparency regarding this decision.  I don't see why they didn't keep the discussion public.</p>
<h2 id="other-potential-problems-untested"><a class="toclink" href="#other-potential-problems-untested">Other potential problems (untested)</a></h2>
<ol>
<li>
<p>A while ago <a href="https://gitlab.com/fdroid/fdroidclient/-/issues/984">I did a deep dive</a> into potential F-Droid info leaks via TLS and HTTP.  I found that they had TLS session resumption enabled, although they thought they had disabled it.  This is a problem because TLS session identifiers can uniquely tag a device much like HTTP cookies can.  F-Droid devs quickly dealt with it by reducing TLS session lifetime to 60 seconds.  /e/ developers should take this into consideration as well, not just for App Lounge, but for any of their apps which make TLS connections.</p>
</li>
<li>
<p>Untested hypothesis: even though CleanAPK no longer sits between the user and Google Play, it may be able to block updates to Google Play apps. This is because if an application is included in both CleanAPK’s and Google Play’s API responses, only the CleanAPK result is displayed to the user. CleanAPK could present a past version of a specific Google Play app to specific users in order to keep them from updating. I have not checked this; I suppose the updater logic could be different from the what-the-user-is-shown logic, so App Lounge might grab the update from Google Play anyway. /e/ devs should check this, at least if their <a href="https://community.e.foundation/t/app-lounge-know-all-about-it/42331#fetching-open-source-applications-from-f-droid-and-get-rid-of-cleanapk-7">plan</a> to cut out CleanAPK completely is not close to being achieved.</p>
</li>
</ol>
<h2 id="final-thoughts"><a class="toclink" href="#final-thoughts">Final thoughts</a></h2>
<p>App Lounge is moving in the right direction, but is currently not as private nor as secure as its alternatives: F-Droid is better at handling free software, Aurora Store is better at dealing with Google Play and PWAs are safer to install by using a web browser and checking the URL.  With more attention paid to security, App Lounge could shine as a PWA installer, but other than that, even after all the <a href="https://community.e.foundation/t/app-lounge-know-all-about-it/42331#whats-next-6">planned improvements</a> are implemented, App Lounge will still be lagging behind F-Droid and Aurora.</p>
<p>Also, from my perspective, the past and current issues with /e/'s app installer do not inspire confidence in /e/OS as a whole, nor do some of /e/'s <a href="https://community.e.foundation/t/advanced-privacy/41395/56">other</a> <a href="https://web.archive.org/web/20200618161905/https://infosec-handbook.eu/blog/e-foundation-final-look/">failures</a>.  For the sake of those who will end up using the system, I hope things will improve, but I've switched to GrapheneOS a while ago and have not looked back since.</p>
<p>The reason I dove into /e/OS in the first place was that I had no better option for my previous phone: the OEM's Android build was spyware and none of my preferred Android distributions had functioning builds for my device.  Were it compatible with GrapheneOS, CalyxOS, DivestOS, LineageOS or Replicant, I would have used one of them instead.  At this point I wonder if I wouldn't have been better off sticking with the stock OS, <a href="https://inteltechniques.com/blog/2022/01/14/the-privacy-security-osint-show-episode-246/">clearing out as much of the junk as possible via adb</a> and using something like <a href="https://f-droid.org/en/packages/eu.faircode.netguard/">NetGuard</a> to block unwanted connections.  Among other advantages, going that route would have preserved <a href="https://source.android.com/docs/security/features/verifiedboot">verified boot</a>, a significant security feature.</p>
<p>The main thing that differentiates /e/ from the above-mentioned Android distros is its well-integrated suite of cloud services, offering many of the conveniences provided by a Google account, but with the same pitfall of storing your data in plaintext on someone else's computer.  If /e/ provided end-to-end encryption for those services (like <a href="https://www.etesync.com/">EteSync</a> does, for instance), it would truly stand out.  Just saying "we're not Google" doesn't cut it for me.</p>
<h2 id="discussion"><a class="toclink" href="#discussion">Discussion</a></h2>
<p>This article has been discussed on <a href="https://community.e.foundation/t/a-critical-review-of-the-app-lounge/44468">the /e/ Foundation forum</a>.</p>
</article>
</main>

<hr/>
<footer class="bottom">
Published: <time datetime="2022-10-09">2022-10-09</time><br/>
Updated: <time datetime="2023-03-29">2023-03-29</time><br/>
<a href="https://gitlab.com/nervuri/nervuri.net/-/blob/master/markdown/e/app-lounge.md">Source</a> (contributions welcome)<br/>
License: <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
</footer>

</body>
</html>
