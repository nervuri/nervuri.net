<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="icon" type="image/ico" href="../favicon.ico"/>
<meta name="theme-color" content="#000"/>
<meta name="color-scheme" content="dark"/>
<meta name="referrer" content="no-referrer"/>
<title>An analysis of the /e/OS app installer | nervuri</title>
<meta name="description" content="Bypassing APK verification in the /e/OS app installer"/>
<link rel="canonical" href="https://nervuri.net/e/apps"/>

<link rel="alternate" type="application/atom+xml" title="nervuri Atom feed" href="../feed.atom"/>

<link rel="pgpkey authn" href="../keys/nervuri-pgp.asc"/>
<link rel="webmention" href="https://webmention.io/nervuri.net/webmention"/>
<link rel="pingback" href="https://webmention.io/nervuri.net/xmlrpc"/>

<link rel="stylesheet" href="../style.css"/>
</head>
<body>

<nav class="top">
	<a href="../.">Home</a> <span class="hidden">|</span>
	<span class="right">
		<a href="../feed.atom" title="subscribe">RSS</a>
	</span>
	<hr/>
</nav>

<main>
<article>
<h1>An analysis of the /e/OS app installer</h1>
<hr />
<div class="toc">
<ul>
<li><a href="#update-2022-10-09">Update – 2022-10-09</a></li>
<li><a href="#summary">Summary</a></li>
<li><a href="#background">Background</a></li>
<li><a href="#how">How?</a></li>
<li><a href="#privacy-issues">Privacy issues</a></li>
<li><a href="#proposed-solutions">Proposed solutions</a></li>
<li><a href="#mitigations-for-users">Mitigations for users</a></li>
<li><a href="#app-sources">App sources</a></li>
<li><a href="#closing-thoughts">Closing thoughts</a></li>
<li><a href="#addendum">Addendum</a><ul>
<li><a href="#contributions">Contributions</a></li>
<li><a href="#discussions">Discussions</a></li>
</ul>
</li>
</ul>
</div>
<hr />
<h2 id="update-2022-10-09"><a class="toclink" href="#update-2022-10-09">Update – 2022-10-09</a></h2>
<p>In May 2022, seven months after this article was published, the /e/ team launched App Lounge, a rewrite of the app installer which downloads applications directly from Google Play.  In <a href="app-lounge">the follow-up</a> I address the changes that it brought.</p>
<h2 id="summary"><a class="toclink" href="#summary">Summary</a></h2>
<p>The <a href="https://e.foundation/">/e/</a> operating system (formerly <a href="https://www.kickstarter.com/projects/290746744/eelo-a-mobile-os-and-web-services-in-the-public-in">eelo</a>) is a privacy-focused variant of Android, forked from <a href="https://lineageos.org/">Lineage OS</a>.  It is part of the <a href="https://gael-duval.medium.com/murena-smartphones-and-cloud-will-protect-our-privacy-357788a1e585">Murena</a> project and is led by <a href="https://en.wikipedia.org/wiki/Ga%C3%ABl_Duval">Gaël Duval</a>, of Mandrake/Mandriva fame.</p>
<p><a href="https://gitlab.e.foundation/e/apps/apps">"Apps"</a>, the /e/ app installer, downloads applications from <a href="https://info.cleanapk.org/">CleanAPK.org</a>, an intermediary which provides apps that originate from <a href="https://f-droid.org/">F-Droid</a> and <a href="#app-sources">elsewhere</a>.</p>
<p>Since apps are not downloaded directly from F-Droid or Google Play, the installer takes certain <a href="https://doc.e.foundation/apps#how-can-i-make-sure--apps-in-the-installer-are-not-tampered-with-but-original">measures</a> to protect against tampering.  Unfortunately, these measures can be bypassed in the majority of cases.  This means that CleanAPK.org (or whoever compromises it) can get maliciously modified apps installed on /e/ users' devices, either when the user is installing a new app or during the update process.</p>
<p>Such an attack can be targeted at specific users, based on device information which the installer reveals to the CleanAPK server every time it checks for updates, namely: the list of installed apps, device model, build ID, Android version and installed languages.  If the installer is configured to install updates automatically (as is the default), CleanAPK can push apps to users' devices in the background.  It can install new apps, but can not replace installed apps with different ones.</p>
<p>These conclusions are based on:</p>
<ul>
<li>intercepting connections made by a build of "Apps" 1.1.6 from April 2021;</li>
<li>testing the attacks on the same build;</li>
<li>an evaluation of code that has been written since to address parts of this problem.</li>
</ul>
<h2 id="background"><a class="toclink" href="#background">Background</a></h2>
<p>According to <a href="https://doc.e.foundation/apps">the FAQ</a> about its built-in app installer,</p>
<blockquote>
<p>The /e/ app installer relies on a third-party application repository called cleanapk.org.</p>
</blockquote>
<p>Apps served by cleanapk.org can be searched <a href="https://e.foundation/e-os-available-applications/">here</a>.</p>
<p>The FAQ also states:</p>
<blockquote>
<p>Apps are checked either using a PGP signature check, or a checksum.</p>
<p>In case you suspect an app to be tampered, we would request you to please report the same here support@e.email and we will take appropriate action keeping you informed.</p>
</blockquote>
<p>I read the code that checked for tampering and found that it could be easily bypassed.  On the 21st of May 2021, I reported this to /e/ with a 90 day publication deadline, which I later extended by another 60 days after finding that <a href="https://gitlab.e.foundation/e/apps/apps/-/commit/26f9ca21c412955d972ee2d61e178701c10835f6">the initial fix</a> did not adequately address the problem.  The team tackled many of the issues I reported (<a href="https://gitlab.e.foundation/e/os/releases/-/releases/v0.19-q">/e/OS v0.19</a> contains the fixes), but as of today (Oct 29 2021), the fundamental problem remains unsolved and the attacks described above are still possible.</p>
<h2 id="how"><a class="toclink" href="#how">How?</a></h2>
<p>CleanAPK.org has a REST API which responds to search queries and returns metadata about available apps.  For an example of how it works, let's say you search for NewPipe, tap the first result, then press "install"; the app installer would make the following API calls (I have removed unessential parameters):</p>
<pre><code>https://api.cleanapk.org/v2/apps?action=search&amp;keyword=newpipe
https://api.cleanapk.org/v2/apps?action=app_detail&amp;id=5b16ba2089bb69289976fbab
https://apk.cleanapk.org/any_50e9e98f0d94f06facfeab7707437300_FDROID.org.schabi.newpipe.apk
https://api.cleanapk.org/v2/apps?action=download&amp;app_id=5b16ba2089bb69289976fbab
</code></pre>
<p>The last call is made redundantly, as the same information is also included in the second call.</p>
<p>On the client side, the installer tries to determine the original source of the app that the user is about to install: /e/ (system app), F-Droid or Google Play.  Based on this, it applies one of 3 checks:</p>
<ul>
<li>system apps have their signatures checked using the system's signing key;</li>
<li>F-Droid apps have their signatures checked using F-Droid's PGP key;</li>
<li>Google Play apps are checked by comparing their SHA1 hashes with those included in the API response; since CleanAPK provides both the files and their hashes, it can tamper with them at will.</li>
</ul>
<p>In order to provide tamper protection for Google Play apps, the installer would need to fetch signing keys from the source, which is difficult, because Google Play does not publish them like F-Droid does.  You generally can't check that the app you got from a third party is the same as on Google Play unless you also download it from Google Play.</p>
<p>Looking at the installer's code, we find that signature verification for F-Droid apps can also be bypassed.  <a href="https://gitlab.e.foundation/e/apps/apps/-/blob/ca5c2507ca7c145e82fab4cc4aa5b889ae8c2196/app/src/main/java/foundation/e/apps/application/model/IntegrityVerificationTask.kt#L51">It used to be the case</a> – and still is, for un-updated systems – that if the API response contained <code>"package_name":"com.google.android.gms"</code>, the installer assumed that the APK file was a system app (MicroG) with the same package name and did no further verification.  And if the response contained a SHA1 hash of the APK file, the installer assumed that it was a Google Play app and did no signature verification.  Both of these assumptions could be leveraged to install malicious apps.</p>
<p><a href="https://gitlab.e.foundation/e/apps/apps/-/blob/5f1f83765bbc5d6526ed252bb516501b6586bf58/app/src/main/java/foundation/e/apps/application/model/IntegrityVerificationTask.kt#L60">Now</a> the app installer extracts the package name from the APK file, then makes a GET request to</p>
<pre><code>https://f-droid.org/en/packages/PACKAGE_NAME/
</code></pre>
<p>to determine if it is an F-Droid app or not.  This means that the f-droid.org server gets to learn about every app that /e/ users are trying to install.  This method also does not protect against instances where the attacker modifies the package name within the APK file.  By modifying the package name, CleanAPK can induce a fallback to the <code>checkGoogleApp()</code> function, which provides no tamper protection.</p>
<p>So, in the end, F-Droid apps from CleanAPK are not protected against tampering either.  CleanAPK can make it appear as though you are installing a libre app like NewPipe and instead you would be installing a malicious version of it (or a different app entirely) with a different internal package name.</p>
<p>System apps are the exception to the rule: they are not downloaded from CleanAPK, but from <a href="https://gitlab.e.foundation/">https://gitlab.e.foundation/</a>.  The only apps that currently fall under <a href="https://gitlab.e.foundation/e/apps/apps/-/blob/5f1f83765bbc5d6526ed252bb516501b6586bf58/app/src/main/assets/systemApp.json">this category</a> are MicroG and Mail.</p>
<p>In Android, once an app is installed, its updates must be signed with the same key.  This means that CleanAPK won't be able to get a malicious APK installed if its package name collides with that of an already installed app.  However, avoiding collisions is easy, especially since CleanAPK receives a list of every user's apps.</p>
<h2 id="privacy-issues"><a class="toclink" href="#privacy-issues">Privacy issues</a></h2>
<p>During update checks (which occur daily, by default), the app installer sends a request to api.cleanapk.org for each installed app, with the exception of pre-installed system apps (like the installer itself).  Each request looks something like this:</p>
<pre><code>GET /v2/apps?action=search&amp;keyword=org.schabi.newpipe&amp;by=package_name
Host: api.cleanapk.org
Accept-Language: en-US,es-VE;q=0.9
User-Agent: Dalvik/2.1.0 (Linux; U; Android 8.0.0; SM-J337U Build/R16NW)
Accept-Encoding: gzip
</code></pre>
<p>The User-Agent header contains the device model, build ID and Android version.  Installed languages are also revealed to CleanAPK via the Accept-Language header.  The information from these headers, along with the IP address and the list of installed apps, can be used to uniquely identify most /e/OS devices.  Aside from privacy, this also affects security, since device fingerprinting can enable targeted attacks.  A compromised CleanAPK.org can quite easily target a specific device to install malware on.</p>
<h2 id="proposed-solutions"><a class="toclink" href="#proposed-solutions">Proposed solutions</a></h2>
<p>My suggestions to /e/OS developers regarding the app installer:</p>
<ol>
<li>Download the entire package index in advance, like F-Droid does;</li>
<li>barring 1, the k-anonymity method used in <a href="https://haveibeenpwned.com/Passwords">https://haveibeenpwned.com/Passwords</a> might also work for privacy-friendly updates;</li>
<li>download apps directly from the source;</li>
<li>barring 3, download vetted signing certificate fingerprints from a server other than CleanAPK's; an initial list of fingerprints can be bundled with the installer, like the F-Droid PGP key currently is;</li>
<li>check that app metadata from the API matches the actual app;</li>
<li>don't send the User-Agent header;</li>
<li>don't send the Accept-Language header; it is currently used to get application metadata in the right language, but the CleanAPK API could be changed to return metadata in all available languages at once.  If compression is used, the size of such responses should be manageable.</li>
</ol>
<p>Also, use a better hash function than SHA1.  SHA256, for instance.</p>
<h2 id="mitigations-for-users"><a class="toclink" href="#mitigations-for-users">Mitigations for users</a></h2>
<p>As mentioned, if the installer is configured to install updates automatically (which it is, by default), CleanAPK can push malicious apps to users' devices in the background.  Also, the installer sends the list of installed apps and other device data to CleanAPK every time it checks for updates.  The "Apps" app cannot be uninstalled or disabled from the Android UI and there is no obvious way to disable update checks.  Still, there are ways for users to limit the reach of the /e/ app installer:</p>
<pre><code>App settings:
  - Update check interval: Monthly
  - Automatically install updates: uncheck

Android Settings &gt; Apps &gt; Apps:
  &gt; FORCE STOP
  &gt; Permissions:
    - Storage: uncheck
  &gt; Data usage:
    - Disable all cellular data access: check
    - Disable all Wi-Fi data access: check
    - Disable all VPN data access: check
</code></pre>
<p>Alternatively, users can uninstall or disable "Apps" (and other system apps) <a href="https://community.e.foundation/t/uninstall-default-apps/8075/29">from the command line</a>:</p>
<pre><code># Uninstall:
adb shell pm uninstall --user 0 foundation.e.apps

# Reinstall:
adb shell cmd package install-existing foundation.e.apps
# If the above does not work, try:
adb shell pm install -r --user 0 /system/app/Apps/Apps.apk

# Disable:
adb shell pm disable-user --user 0 foundation.e.apps

# Enable:
adb shell pm enable foundation.e.apps
</code></pre>
<p>You can also block internet connections for /e/ "Apps" using a firewall such as <a href="https://f-droid.org/en/packages/eu.faircode.netguard/">NetGuard</a>.  Or you can block connections to CleanAPK by enabling adb root in the developer options and adding the following to <code>/system/etc/hosts</code>:</p>
<pre><code>0.0.0.0 api.cleanapk.org
0.0.0.0 apk.cleanapk.org
</code></pre>
<p>F-Droid and Aurora Store can be used instead to give you access to pretty much the same broad range of apps, but directly from the source.  You can install F-Droid from <a href="https://f-droid.org/">the official website</a> and Aurora from <a href="https://auroraoss.com/">its official website</a> or from F-Droid.</p>
<p>/e/ "Apps" provides an embedded privacy score (calculated by CleanAPK) and list of trackers (detected by <a href="https://exodus-privacy.eu.org/en/">Exodus Privacy</a>) for each app.  However, the way in which CleanAPK calculates the privacy score is not clearly documented and I found it to be misleading at times (for instance, Tor Browser has a lower privacy score than Facebook Lite).  Aurora Store also provides a list of trackers detected by Exodus Privacy.  As for F-Droid, it lets you know when an app <em>tracks and reports your activity</em>, which <a href="https://www.f-droid.org/en/2020/01/16/tracking-the-trackers.html">is determined</a> using a computer-assisted human review in which an Exodus Privacy report is taken into account.  F-Droid checks for <a href="https://f-droid.org/en/docs/Anti-Features/">other anti-features</a> as well.</p>
<h2 id="app-sources"><a class="toclink" href="#app-sources">App sources</a></h2>
<p>Other than F-Droid, it is not clear what other source(s) CleanAPK uses.  But there is at least one sign that it uses <a href="https://apkpure.com/">APKPure.com</a>:</p>
<pre><code>https://api.cleanapk.org/v1/apps?action=app_detail&amp;id=5beee6c54ecab4722bde0e28
</code></pre>
<p>This API call returns metadata about the Uber app.  The response contains: <code>"apk_App uploaded by:":"Mòbile Soe"</code>.  The <a href="https://apkpure.com/uber-request-a-ride/com.ubercab">APKPure page for Uber</a> also contains <code>"App uploaded by: Mòbile Soe"</code>.  The same is also true for other proprietary apps served by CleanAPK, but not all.</p>
<p>If APKPure is used as a source, then we're relying on two intermediaries, not just CleanAPK.  So it's even more important for the app installer to have proper tamper protections in place.</p>
<p>Maybe CleanAPK also gets apps from Google Play directly, or maybe not.  It would be good to have transparency on this.</p>
<h2 id="closing-thoughts"><a class="toclink" href="#closing-thoughts">Closing thoughts</a></h2>
<p>I can't shake the sense that the /e/ team is stretched too thin.  The scope of the /e/ project is huge: maintaining online services, maintaining their own apps and forks, supporting around 200 devices, etc.  They have an enormous amount of work on their hands.  So, even though it is important, I can't say I'm surprised by the lack of care given to app verification, nor by other signs of rushed work.</p>
<p>But since /e/ is focused on privacy, I am somewhat surprised by the data it leaks.  You would think that, before claiming privacy, such a project would analyze and disclose its own network connections.  But this is <a href="https://web.archive.org/web/20200618161905/https://infosec-handbook.eu/blog/e-foundation-final-look/">not the first time</a> /e/ has been shown to fail in this regard.  Users deserve to know exactly what data is sent where and who can access it.</p>
<p>That being said, let's not lose sight of the fact that /e/ is significantly more privacy-respecting than the average Android system.  Also, the problems I have described can be solved and based on my communications with the team, it appears they will be.</p>
<h2 id="addendum"><a class="toclink" href="#addendum">Addendum</a></h2>
<h3 id="contributions"><a class="toclink" href="#contributions">Contributions</a></h3>
<p>Thanks to Arnau Vàzquez Palma, Hans-Christoph Steiner and Pete Fotheringham for pointing out a few <a href="https://community.e.foundation/t/an-analysis-of-the-e-os-app-installer/35802/12">errors</a> in the initial version of the article.  They have been corrected.</p>
<h3 id="discussions"><a class="toclink" href="#discussions">Discussions</a></h3>
<p>This article has been discussed on:</p>
<ul>
<li><a href="https://community.e.foundation/t/an-analysis-of-the-e-os-app-installer/35802">The /e/ Foundation forum</a></li>
<li><a href="https://forum.f-droid.org/t/an-analysis-of-the-e-os-app-installer/15700">The F-Droid forum</a></li>
</ul>
</article>
</main>

<hr/>
<footer class="bottom">
Published: <time datetime="2021-10-29">2021-10-29</time><br/>
Updated: <time datetime="2022-10-09">2022-10-09</time><br/>
<a href="https://gitlab.com/nervuri/nervuri.net/-/blob/master/markdown/e/apps.md">Source</a> (contributions welcome)<br/>
License: <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
</footer>

</body>
</html>
