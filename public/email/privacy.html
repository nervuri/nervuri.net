<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="icon" type="image/ico" href="../favicon.ico"/>
<meta name="theme-color" content="#000"/>
<meta name="color-scheme" content="dark"/>
<meta name="referrer" content="no-referrer"/>
<title>Notes on e-mail privacy | nervuri</title>
<meta name="description" content="This article delves into e-mail privacy problems and proposes solutions focused on minimizing reliance on the e-mail service provider."/>
<link rel="canonical" href="https://nervuri.net/email/privacy"/>

<link rel="alternate" type="application/atom+xml" title="nervuri Atom feed" href="../feed.atom"/>

<link rel="pgpkey authn" href="../keys/nervuri-pgp.asc"/>
<link rel="webmention" href="https://webmention.io/nervuri.net/webmention"/>
<link rel="pingback" href="https://webmention.io/nervuri.net/xmlrpc"/>

<link rel="stylesheet" href="../style.css"/>
</head>
<body>

<nav class="top">
	<a href="../.">Home</a> <span class="hidden">|</span>
	<span class="right">
		<a href="gemini://rawtext.club/~nervuri/email/privacy.gmi">Gemini</a> |
		<a href="../feed.atom" title="subscribe">RSS</a>
	</span>
	<hr/>
</nav>

<main>
<article>
<h1>Notes on e-mail privacy</h1>
<hr />
<div class="toc">
<ul>
<li style="list-style-type:'» ';"> <a href="#introduction">Introduction</a></li>
<li style="list-style-type:'» ';"> <a href="#pop3-considerations">POP3 considerations</a></li>
<li style="list-style-type:'» ';"> <a href="#pop-like-imap">POP-like IMAP</a></li>
<li style="list-style-type:'» ';"> <a href="#caveats">Caveats</a></li>
<li style="list-style-type:'» ';"> <a href="#starttls-vs-implicit-tls">STARTTLS vs implicit TLS</a></li>
<li style="list-style-type:'» ';"> <a href="#synchronization-and-backup">Synchronization and backup</a></li>
<li style="list-style-type:'» ';"> <a href="#contacts-and-calendars">Contacts and calendars</a></li>
<li style="list-style-type:'» ';"> <a href="#e-mail-headers">E-mail headers</a></li>
<li style="list-style-type:'» ';"> <a href="#e-mail-clients">E-mail clients</a><ul>
<li style="list-style-type:'» ';"> <a href="#mutt-mpop-msmtp-and-getmail">Mutt, mpop, msmtp and getmail</a></li>
<li style="list-style-type:'» ';"> <a href="#thunderbird">Thunderbird</a></li>
</ul>
</li>
<li style="list-style-type:'» ';"> <a href="#throwaway-addresses-aliases-catch-all">Throwaway addresses, aliases, catch-all</a></li>
<li style="list-style-type:'» ';"> <a href="#tracking-links">Tracking links</a></li>
<li style="list-style-type:'» ';"> <a href="#attach">Attach?</a></li>
<li style="list-style-type:'» ';"> <a href="#end-to-end-encryption">End-to-end encryption</a></li>
<li style="list-style-type:'» ';"> <a href="#closing-thoughts">Closing thoughts</a></li>
<li style="list-style-type:'» ';"> <a href="#contributions">Contributions</a></li>
</ul>
</div>
<hr />
<h2 id="introduction"><a class="toclink" href="#introduction">Introduction</a></h2>
<p>E-mail privacy is notoriously bad:</p>
<ul>
<li>Most people use parasitic e-mail providers with anti-privacy business models.</li>
<li>Most people accumulate decades' worth of messages server-side.</li>
<li>Many e-mail servers expose the sender's IP address within a message header (because the SMTP specification says they "MUST").</li>
<li>TLS is optional for server-to-server connections.  Even when used, it is most often unauthenticated.  Most servers don't support DANE or MTA-STS, but do support deprecated TLS versions and weak cipher suites.</li>
<li>End-to-end encryption is generally a pain to use, so hardly anyone does.</li>
<li>End-to-end encryption only covers the message body, not the subject nor any of the other headers.</li>
<li>E-mail clients add extraneous headers which expose information about the user.</li>
<li>HTML e-mail is a thing, therefore tracking e-mail views via HTML is also a thing.</li>
<li>URLs with unique identifiers are widely used for tracking.</li>
<li>Et cetera.</li>
</ul>
<p>What bothers me most is that IMAP and webmail have lead to billions of people and organizations accumulating years and years' worth of private messages on mail servers.  This increased their risk of data compromise to the point of near-certainty.  Webmail in particular made people dependent on their online accounts for accessing their e-mail archives.  And providers make zero effort to persuade people to move their data offline, even though this could be done with a couple of clicks in the webmail interface.</p>
<p>Given this state of affairs, it's worth figuring out how to minimize our information exposure.  On that note, here are a few features of my e-mail setup:</p>
<ul>
<li>E-mail is downloaded over POP3, then deleted from the server immediately.</li>
<li>Connections to the server are made over TLS 1.3 only (no STARTTLS).</li>
<li>All TLS connections are authenticated via certificate pinning.</li>
<li>All connections are made over Tor.</li>
<li>E-mail filtering is done client-side, no filtering information is stored on the server.</li>
<li>Messages are rendered as plain text by default, but if HTML is required, all HTML (anti-)features that can be used for tracking are disabled.</li>
<li>When sending e-mail, my timezone is spoofed.  The e-mail client does not add a User-Agent header to the message, nor any other privacy-sensitive headers like Content-Language.</li>
</ul>
<p>This is desiderata I have irrespective of which e-mail service I'm using.  Filtering, storing and synchronizing e-mail across multiple devices should be the client's responsibility, while the server should know and do as little as possible (namely: receive messages, encrypt them to the user's public key – ideally – and store them until fetched).</p>
<p>Hardly anyone uses POP anymore, but returning to POP's download-and-delete approach would, in principle, be appropriate for most use cases, as software for end-to-end encrypted file synchronization can supplant IMAP's sync functionality – and could also synchronize e-mail filters and various client settings.  Current mail clients don't make this approach easy in practice, but it's worth striving towards (by client developers in particular), as it's fundamentally more secure and private than the dominant server-centric paradigm.</p>
<h2 id="pop3-considerations"><a class="toclink" href="#pop3-considerations">POP3 considerations</a></h2>
<p>When using POP, there are a few things to keep in mind.  Most importantly: POP3 only downloads messages from the Inbox folder, so any server-side filters for putting specific messages in specific folders will have to be moved client-side.  Filters and folder names can contain sensitive information, so moving them off the server is a good idea anyway.  Client-side filters can either be configured in the e-mail client itself, or in a separate program like fdm.</p>
<p>However, there is one filter you might want to add server-side: "save all mail to inbox" (including spam).  Otherwise your client will not download messages marked as spam and you won't get a chance to see the wrongly-flagged ones.  Spam filtering can be configured client-side as well, either by making use of headers added by the server's spam checker, or by running a spam checker yourself.  Thunderbird, for example, comes with a built-in junk classifier and ClawsMail can integrate with SpamAssasin and Bogofilter via plugins.</p>
<p>Another issue is that e-mail service providers sometimes don't allow POP3 to work as it's supposed to.  For example, when using Microsoft's e-mail service (Outlook/Hotmail), you need to explicitly allow the POP3 client to delete messages (Settings -&gt; Mail -&gt; Sync email -&gt; POP options), otherwise they'll just be moved to another folder.  In GMail, POP3 has to be explicitly enabled:</p>
<p>» <a href="https://support.google.com/mail/answer/7104828?hl=en">Read Gmail messages on other email clients using POP</a></p>
<p>Some privacy-focused providers like Proton Mail and Tutanota require special client software and do not support POP3.  There is bridging software available for using Proton Mail with regular IMAP and SMTP, but there's currently no such software for Tutanota.</p>
<p>» <a href="https://github.com/emersion/hydroxide">hydroxide, third-party Proton Mail bridge</a><br />
» <a href="https://github.com/ProtonMail/proton-bridge">official Proton Mail bridge</a></p>
<h2 id="pop-like-imap"><a class="toclink" href="#pop-like-imap">POP-like IMAP</a></h2>
<p>IMAP can be used in a POP-like fashion as well, without being limited to the Inbox folder, but most clients don't support this mode of operation.  I've only seen this feature in getmail, which has several <code>delete_*</code> options to control the removal of messages from the server after they're fetched.  Deletion does not have to be immediate; getmail can be set to remove messages a specific number of days after they are downloaded, which means that you can still access recent messages on other computers via webmail or IMAP, and if the server gets compromised, the attacker won't get most of your mail.</p>
<p>» <a href="https://getmail6.org/configuration.html#conf-options">getmail 6</a></p>
<p>When moving your e-mail archive offline, you'll need to use IMAP in order to preserve the folder structure.  Beyond that, however, bear in mind that POP3 is much simpler, has a smaller attack surface, and works in fetch-and-delete manner with every e-mail client that supports it.</p>
<h2 id="caveats"><a class="toclink" href="#caveats">Caveats</a></h2>
<p>Bear in mind:</p>
<ul>
<li>There is no guarantee that e-mail providers don't keep copies of deleted messages.  Even if nothing shady is going on, it is likely that e-mail is kept in server backups for a period of time, and the same goes for message metadata in server logs.  The specific period ought to be disclosed in each service provider's privacy policy.</li>
<li>Mainstream providers like Google and Microsoft may not allow connections over Tor and may even block VPNs, so you might have to reveal your real IP address to them.  If you do, make sure to check whether they expose it in mail headers.</li>
</ul>
<h2 id="starttls-vs-implicit-tls"><a class="toclink" href="#starttls-vs-implicit-tls">STARTTLS vs implicit TLS</a></h2>
<p>It's official: STARTTLS is to be avoided.  Appendix A of RFC 8314 details why, after a long period in which implicit TLS for e-mail protocols was deemed deprecated, it is now the recommended option.  In their 2021 Usenix paper on the topic, Damian Poddebniak et al. provided a jaw-dropping illustration of how prone STARTTLS is to implementation flaws.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://datatracker.ietf.org/doc/html/rfc8314">RFC 8314 – Cleartext Considered Obsolete: Use of Transport Layer Security (TLS) for Email Submission and Access (2018)</a></li>
<li style="list-style-type:'» ';"> <a href="https://www.usenix.org/conference/usenixsecurity21/presentation/poddebniak">Why TLS is better without STARTTLS: A Security Analysis of STARTTLS in the Email Context (2021)</a></li>
</ul>
<p>In short, use:</p>
<ul>
<li>SMTP over port 465 rather than 587</li>
<li>POP3 over port 995 rather than 110</li>
<li>IMAP over port 993 rather than 143</li>
</ul>
<h2 id="synchronization-and-backup"><a class="toclink" href="#synchronization-and-backup">Synchronization and backup</a></h2>
<p>For synchronizing e-mail messages (and any other files) between devices in real time, there are several privacy-preserving solutions.  Proprietary programs aside, the easiest option might be Nextcloud with end-to-end encryption enabled.  You don't need to self-host, there are many Nextcloud providers, some of which offer a few GB of storage at no cost.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://nextcloud.com/endtoend/">Nextcloud end-to-end encryption</a></li>
<li style="list-style-type:'» ';"> <a href="https://nextcloud.com/sign-up/">Nextcloud providers</a></li>
</ul>
<p>Then there's Syncthing, an excellent program for peer-to-peer sync:</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://syncthing.net/">Syncthing</a></li>
</ul>
<p>The problem with peer-to-peer sync is that the devices you're synchronizing need to be online at the same time.  You can work around this by also synchronizing to an "always-online" device (a server, a phone, etc).  If you don't fully trust this device, you can hide your files from it via some form of transparent file encryption.  Syncthing supports this via its "untrusted devices" feature, which is still in beta phase, but has been around for a while and reportedly works well.  I used eCryptfs instead; it is Linux-only, but there are cross-platform solutions as well, such as EncFS, securefs, CryFS and Cryptomator.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://docs.syncthing.net/users/untrusted.html">Syncthing – Untrusted (Encrypted) Devices</a></li>
<li style="list-style-type:'» ';"> <a href="https://nuetzlich.net/gocryptfs/comparison/">Comparison table: transparent file encryption software</a></li>
<li style="list-style-type:'» ';"> <a href="https://mhogomchungu.github.io/sirikali/">SiriKali's list of encryption backends</a></li>
</ul>
<p>My setup involved a phone and two computers synchronizing an encrypted folder via Syncthing; the folder was opaque to my phone, but the computers mounted it automatically on login, and synchronization was seamless.  I took these notes when setting up eCryptfs:</p>
<pre><code class="language-shell"># basic setup of ~/Private/ (and ~/.Private/), auto-mounted on login:
sudo apt install ecryptfs-utils
sudo modprobe ecryptfs
ecryptfs-setup-private

# setup of ~/Private/ (and ~/.Private/) WITHOUT auto-mount on login:
ecryptfs-setup-private --nopwcheck --noautomount --wrapping
ecryptfs-mount-private
ecryptfs-umount-private

# to change the passphrase:
ecryptfs-rewrap-passphrase ~/.ecryptfs/wrapped-passphrase
</code></pre>
<ul>
<li style="list-style-type:'» ';"> <a href="https://wiki.archlinux.org/index.php/ECryptfs">eCryptfs page on the Arch Linux wiki</a></li>
</ul>
<p>At the moment I don't need real-time synchronization between devices.  I rely on my backup if I have to move to a different machine.</p>
<p>When it comes to backup, I've used Borg and Duplicati and have heard good things about restic.  All three support encryption, so backups can be safely stored on other people's machines.  Duplicati is the more beginner-friendly one, it has a graphical interface and works on Windows.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://www.borgbackup.org/">Borg</a></li>
<li style="list-style-type:'» ';"> <a href="https://restic.net/">restic</a></li>
<li style="list-style-type:'» ';"> <a href="https://www.duplicati.com/">Duplicati</a></li>
</ul>
<p>Borg is easy to set up and has great documentation.  This is how I use it to back up important files to my Raspberry Pi via SSH:</p>
<pre><code class="language-shell">export BORG_REPO=myraspberrypi:/home/backup/mystuff
BORG_PASSPHRASE=&quot;$(pass show bkp)&quot;
export BORG_PASSPHRASE

borg create --progress --stats ::&quot;{now:%Y-%m-%d}&quot; ~/Documents ~/Mail
borg prune --verbose --list --keep-daily=7 --keep-weekly=4
</code></pre>
<p>The Pi then rsyncs the backup to an off-site server.</p>
<h2 id="contacts-and-calendars"><a class="toclink" href="#contacts-and-calendars">Contacts and calendars</a></h2>
<p>Vdirsyncer and DecSync CC can be used for synchronizing calendars and contacts as files.  Alternatively, EteSync can be used for end-to-end encrypted and history-preserving synchronization of contacts and calendars.</p>
<p>Vdirsyncer syncs between a CardDAV/CalDAV server and the local filesystem; it runs on GNU/Linux and macOS.  DecSync CC is an Android contacts &amp; calendar provider which uses a local directory instead of a server; the directory can be synchronized using a separate program (see above).  EteSync is a cross-platform server-based solution with end-to-end encryption, which also saves the history of changes made to your calendar and contacts.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://vdirsyncer.pimutils.org/en/stable/">vdirsyncer</a></li>
<li style="list-style-type:'» ';"> <a href="https://github.com/39aldo39/DecSyncCC">DecSync CC</a></li>
<li style="list-style-type:'» ';"> <a href="https://www.etesync.com/">EteSync</a></li>
</ul>
<p>I, for one, back up my contacts from my phone to my laptop manually.  As for the calendar, I only access it on my laptop.  Real-time synchronization is overkill for my needs.</p>
<h2 id="e-mail-headers"><a class="toclink" href="#e-mail-headers">E-mail headers</a></h2>
<p>Here are a few examples of client-added e-mail headers and what information they expose:</p>
<ul>
<li>User-Agent: reveals the mail client's name and version; may also include the operating system's name and version.</li>
<li>X-Mailer: client name and version.</li>
<li>Content-Language: languages your client is configured to use (for spellchecking and such).</li>
<li>Date: system time, including timezone.</li>
<li>Message-ID: may reveal the local machine's hostname and/or system time.  The algorithm for generating message IDs varies from client to client, so it can also be used to deduce what mail program you use (Thunderbird uses UUIDs, Mutt uses a Unix timestamp concatenated with a random number, etc).</li>
<li>Content-Type: reveals whether or not you use format=flowed, which may stand out if it's not your client's default setting.</li>
</ul>
<p>The order of headers can also be used for fingerprinting, as well as differences in how multi-line headers are indented.  There may also be other subtle differences between how the various e-mail clients generate headers.</p>
<h2 id="e-mail-clients"><a class="toclink" href="#e-mail-clients">E-mail clients</a></h2>
<h3 id="mutt-mpop-msmtp-and-getmail"><a class="toclink" href="#mutt-mpop-msmtp-and-getmail">Mutt, mpop, msmtp and getmail</a></h3>
<p>My current e-mail setup consists of mutt, mpop, msmtp and a small bespoke filtering script that took the place of fdm.  When I need IMAP, I use getmail.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="http://www.mutt.org/">mutt</a></li>
<li style="list-style-type:'» ';"> <a href="https://marlam.de/mpop/">mpop</a></li>
<li style="list-style-type:'» ';"> <a href="https://marlam.de/msmtp/">msmtp</a></li>
<li style="list-style-type:'» ';"> <a href="https://getmail6.org/">getmail 6</a></li>
<li style="list-style-type:'» ';"> <a href="https://tildegit.org/nervuri/email-configs">my configs and filtering script</a></li>
</ul>
<p>I configured mpop and msmtp to retrieve mail account passwords from pass.  You could also use a variant of pass called passage that uses age instead of GPG.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://www.passwordstore.org/">pass</a></li>
<li style="list-style-type:'» ';"> <a href="https://github.com/FiloSottile/passage">passage</a></li>
</ul>
<h3 id="thunderbird"><a class="toclink" href="#thunderbird">Thunderbird</a></h3>
<p>A more approachable solution for non-techies is to use Thunderbird.  The procedure for moving e-mail offline is simple: add your account via IMAP and then drag each folder to the Local Folders section.  After the transfer completes, double-check that the number of messages in your local folders is the same as on the IMAP side, then delete all IMAP folder contents.  Messages will be stored in your Thunderbird profile directory, which you can then back up:</p>
<p>» <a href="https://github.com/HorlogeSkynet/thunderbird-user.js/wiki/1.2-Backing-Up">How to back up your Thunderbird profile directory</a></p>
<p>Before adding any accounts to Thunderbird, you may want to set the following in Settings -&gt; General -&gt; Config Editor (at the bottom):</p>
<pre><code class="language-Thunderbird_settings"># check all folders for new messages (including spam and trash)
mail.server.default.check_all_folders_for_new = true

# change default sort order (new messages first)
mailnews.default_sort_order = 2
mailnews.default_news_sort_order = 2
</code></pre>
<p>Thunderbird does not have good privacy protections by default, but it can be configured to get you most of the way there.  At a minimum, I would set the following:</p>
<pre><code class="language-Thunderbird_settings"># hide timezone
mail.sanitize_date_header = true

# don't add Content-Language header
mail.suppress_content_language = true

# don't add User-Agent header
general.useragent.override = &quot;&quot;
</code></pre>
<p>You can also go to "Settings -&gt; General -&gt; Connection" and configure Thunderbird to proxy traffic through Tor.  That's the equivalent of:</p>
<pre><code class="language-Thunderbird_settings">network.proxy.socks = 127.0.0.1
network.proxy.socks_port = 9050
network.proxy.socks_remote_dns = true
</code></pre>
<p>For more in-depth Thunderbird hardening, HorlogeSkynet's user.js file is worth looking into:</p>
<p>» <a href="https://github.com/HorlogeSkynet/thunderbird-user.js">Thunderbird user.js</a></p>
<p>This is not to say I recommend it in its entirety.  For instance, it sets <code>privacy.resistFingerprinting = true</code>, which paradoxically makes Thunderbird more fingerprintable by adding a Firefox-specific User-Agent header to all outgoing e-mail.  This setting was made for Firefox and does not work well with Thunderbird.</p>
<p>See also:</p>
<p>» <a href="https://superuser.com/questions/1672309/how-do-i-disable-telemetry-in-thunderbird">How do I disable telemetry in Thunderbird?</a></p>
<h2 id="throwaway-addresses-aliases-catch-all"><a class="toclink" href="#throwaway-addresses-aliases-catch-all">Throwaway addresses, aliases, catch-all</a></h2>
<p>The e-mail address is an identifier that links one's online accounts.  I generally don't want them linked, so most often I don't reveal my real e-mail address to services I sign up to.</p>
<p>When I need a throwaway address for an unimportant account, I reach for anonbox, Guerilla Mail or (for longer-term addresses) danwin1210.de and Fedora Email (not affiliated with the distro).  I use SimpleLogin when I want messages to be forwarded to one of my real addresses.  In cases where I don't want messages going through a third-party e-mail forwarder, I fall back to catch-all e-mail on my own domain.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://anonbox.net/">anonbox</a></li>
<li style="list-style-type:'» ';"> <a href="https://www.guerrillamail.com/">Guerilla Mail</a></li>
<li style="list-style-type:'» ';"> <a href="https://danwin1210.de/mail/">danwin1210.de</a></li>
<li style="list-style-type:'» ';"> <a href="https://fedora.email/">Fedora Email</a></li>
<li style="list-style-type:'» ';"> <a href="https://simplelogin.io/">SimpleLogin</a></li>
</ul>
<p>If you start receiving spam, using a unique address for each service allows you to identify which one leaked your address to the spammers.  It also enables you to block addresses that are receiving unwanted messages.</p>
<h2 id="tracking-links"><a class="toclink" href="#tracking-links">Tracking links</a></h2>
<p>Messages sent by companies very often include links with tracking parameters.  Often these parameters can be stripped without breaking the link, which is something that mail clients can do automatically, to some extent.  For instance, Proton Mail has this feature:</p>
<p>» <a href="https://proton.me/blog/tracking-links-protection">Proton Mail tracking links protection</a></p>
<p>Thunderbird inherits a similar feature from Firefox:</p>
<p>» <a href="https://firefox-source-docs.mozilla.org/toolkit/components/antitracking/anti-tracking/query-stripping/index.html">Firefox Query Parameter Stripping</a></p>
<pre><code>privacy.query_stripping.enabled = true
privacy.query_stripping.enabled.pbmode = true
</code></pre>
<p>Proton Mail also prompts users when they click links, displaying the links in full and asking for confirmation before opening them.  This is meant to protect against phishing and could be enhanced by presenting the link in way that's easier to read, highlighting the hostname.</p>
<p>The more mail clients incorporate such features, the better.</p>
<p>» <a href="https://fmarier.github.io/brave-testing/query-filter.html">Tracking link test page</a></p>
<h2 id="attach"><a class="toclink" href="#attach">Attach?</a></h2>
<p>Sometimes institutions and companies request sensitive information via e-mail.  You may be asked to attach a photo of your ID, for example.  Consider that, no matter what their privacy policies say, the number of organizations with good e-mail hygiene is close to zero.  Messages remain on their servers (probably owned and managed by third parties) for who knows how long, increasing the odds that your data will be compromised.  So, if possible, send an ephemeral link to the document instead.  Either link to the file on a server you control or use a service which supports in-browser encryption.  I have used:</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://send.vis.ee/">send.vis.ee (fork of Firefox Send)</a></li>
<li style="list-style-type:'» ';"> <a href="https://upload.disroot.org/">Lufi – Disroot file uploader</a></li>
<li style="list-style-type:'» ';"> <a href="https://send.tresorit.com/">send.tresorit.com</a></li>
<li style="list-style-type:'» ';"> <a href="https://mega.io/">mega.io</a></li>
</ul>
<p>Some employees are trained never to click links in e-mail, so this may not always work, but it's worth trying.  Also consider that it may be more private to present the information in person or send it via postal mail.  Paper is better for privacy; digital copies tend to multiply.</p>
<h2 id="end-to-end-encryption"><a class="toclink" href="#end-to-end-encryption">End-to-end encryption</a></h2>
<p>It's strange to me that so many e-mail privacy guides jump straight to PGP when there are much lower-hanging fruit that are more beneficial to most people's privacy.  Still, end-to-end encryption (via PGP, S/MIME, codecrypt, etc) is important in some contexts and should be used when appropriate.</p>
<p>There have been developments on the PGP UX front.  For one thing, Proton Mail made PGP work such that users don't need to think about it or even know what it is.  Proton Mail supports PGP encryption to external servers as well, with key discovery done automatically via WKD.  The Pretty Easy Privacy project has also made noteworthy advances on the PGP UX front, as has Mailvelope.  Researchers may continue to publish papers about how Johnny can't encrypt, but the tools have clearly gotten better and can get better still.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://proton.me/blog/security-updates-2019">Proton Mail supports PGP WKD</a></li>
<li style="list-style-type:'» ';"> <a href="https://pep.foundation/">p≡p (pretty Easy privacy) Foundation</a></li>
<li style="list-style-type:'» ';"> <a href="https://mailvelope.com/">Mailvelope</a></li>
<li style="list-style-type:'» ';"> <a href="https://arxiv.org/pdf/1510.08555.pdf">Why Johnny Still, Still Can’t Encrypt</a></li>
</ul>
<p>Modern keyservers like keys.openpgp.org send verification e-mails to check if the key owner really controls the e-mail address(es) associated with the key.  Going further, projects like Keyoxide and Keybase offer proofs of online identity by associating public accounts and website(s) with PGP keys in a verifiable way.  This helps greatly when assessing whether a particular key belongs to who it's supposed to.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://keys.openpgp.org/">keys.openpgp.org</a></li>
<li style="list-style-type:'» ';"> <a href="https://keyoxide.org/">Keyoxide</a></li>
<li style="list-style-type:'» ';"> <a href="https://keybase.io/">Keybase</a></li>
</ul>
<p>The Memory Hole project was a standardization effort that addressed PGP encryption and/or signing of e-mail headers.  The proposed standard was implemented by programs such as Enigmail and Mailpile.  The most recent push in this regard is the IETF draft "Protected Headers for Cryptographic E-mail", the last revision of which was published in December 2019.  Mutt, for example, has implemented coverage for the Subject header (see the <code>crypt_protected_headers_read</code> option).</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://modernpgp.org/memoryhole/">Memory Hole</a></li>
<li style="list-style-type:'» ';"> <a href="https://datatracker.ietf.org/doc/draft-autocrypt-lamps-protected-headers/">Protected Headers for Cryptographic E-mail</a></li>
</ul>
<p>There is also a standard called Autocrypt for negotiating end-to-end encryption between two users of e-mail:</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://autocrypt.org/">Autocrypt</a></li>
</ul>
<p>Lastly, there is software for e-mail servers which automatically encrypts incoming mail to the recipient's PGP key.  This is not end-to-end encryption, but it's noteworthy nonetheless.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://git.disroot.org/Disroot/gpg-lacre">Lacre</a></li>
</ul>
<p>Further reading/viewing:</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://onion.tube/watch?v=2vFMSJLaOOQ">Jens Müller – Covert Attacks on Email End-to-End Encryption</a></li>
<li style="list-style-type:'» ';"> <a href="https://efail.de/">EFAIL</a></li>
<li style="list-style-type:'» ';"> <a href="https://arstechnica.com/information-technology/2016/12/signal-does-not-replace-pgp/">Neal H. Walfield – Why I’m not giving up on PGP</a></li>
<li style="list-style-type:'» ';"> <a href="https://datatracker.ietf.org/doc/html/draft-brown-pgp-pfs-03">Forward Secrecy Extensions for OpenPGP</a></li>
</ul>
<h2 id="closing-thoughts"><a class="toclink" href="#closing-thoughts">Closing thoughts</a></h2>
<p>What I'm ultimately hoping for is the development of a newbie-friendly private-by-default e-mail client which nails it on every privacy front.  One such effort worth keeping an eye on is Mailpile, which is currently being overhauled with help from NLnet.</p>
<p>» <a href="https://www.mailpile.is/blog/2023-05-01_A_Mail_Client_in_Six_Steps.html">Mailpile 2 – "A Mail Client in Six Steps"</a></p>
<p>I'd also like to see better integration between server and client, such that the client is able to:</p>
<ul>
<li>show the mail service provider (MX records) for each destination address;</li>
<li>show if the server is going to use TLS for the connection to the destination server and what kind of authentication (if any) it will perform;</li>
<li>set per-domain TLS policies (see Postfix's <code>smtp_tls_policy_maps</code>);</li>
<li>inform the user when a message has been deferred (left in the server queue), what the reason was, when the retries are scheduled to occur, and notify the user when it gets delivered (in case of greylisting, for instance).</li>
</ul>
<p>That's about it.</p>
<p>P.S.
» <a href="https://useplaintext.email/">Use plain text email.</a></p>
<hr />
<h2 id="contributions"><a class="toclink" href="#contributions">Contributions</a></h2>
<p>Thanks to sl1200 for letting me know about Syncthing's "untrusted devices" feature and reminding me of Keyoxide.</p>
<p>» <a href="https://dystopic.world/">sl1200</a></p>
</article>
</main>

<hr/>
<footer class="bottom">
Published: <time datetime="2023-08-11">2023-08-11</time><br/>
Updated: <time datetime="2023-11-01">2023-11-01</time><br/>
<a href="https://gitlab.com/nervuri/nervuri.net/-/blob/master/markdown/email/privacy.md">Source</a> (contributions welcome)<br/>
License: <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>
</footer>

</body>
</html>
