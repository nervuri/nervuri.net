<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="icon" type="image/ico" href="favicon.ico"/>
<meta name="theme-color" content="#000"/>
<meta name="color-scheme" content="dark"/>
<meta name="referrer" content="no-referrer"/>
<title>TLS Client Hello Mirror | nervuri</title>
<meta name="description" content="Client Hello Mirror is a server which outputs your browser's TLS Client Hello message, emphasizing aspects of it that are detrimental to privacy."/>
<link rel="canonical" href="https://nervuri.net/client-hello-mirror"/>

<link rel="alternate" type="application/atom+xml" title="nervuri Atom feed" href="feed.atom"/>

<link rel="pgpkey authn" href="keys/nervuri-pgp.asc"/>
<link rel="webmention" href="https://webmention.io/nervuri.net/webmention"/>
<link rel="pingback" href="https://webmention.io/nervuri.net/xmlrpc"/>

<link rel="stylesheet" href="style.css"/>
</head>
<body>

<nav class="top">
	<a href=".">Home</a> <span class="hidden">|</span>
	<span class="right">
		<a href="gemini://rawtext.club/~nervuri/client-hello-mirror.gmi">Gemini</a> |
		<a href="feed.atom" title="subscribe">RSS</a>
	</span>
	<hr/>
</nav>

<main>
<article>
<h1>TLS Client Hello Mirror</h1>
<hr />
<div class="toc">
<ul>
<li style="list-style-type:'» ';"> <a href="#what">What</a></li>
<li style="list-style-type:'» ';"> <a href="#why">Why</a></li>
<li style="list-style-type:'» ';"> <a href="#how">How</a><ul>
<li style="list-style-type:'» ';"> <a href="#first-steps">First steps</a></li>
<li style="list-style-type:'» ';"> <a href="#cve-2022-30629">CVE-2022-30629</a></li>
<li style="list-style-type:'» ';"> <a href="#privileges-and-timeouts">Privileges and timeouts</a></li>
<li style="list-style-type:'» ';"> <a href="#tlshelloagwaname">tlshello.agwa.name</a></li>
<li style="list-style-type:'» ';"> <a href="#nja3">NJA3</a></li>
</ul>
</li>
<li style="list-style-type:'» ';"> <a href="#mentions">Mentions</a></li>
<li style="list-style-type:'» ';"> <a href="#closing-thoughts">Closing thoughts</a></li>
</ul>
</div>
<hr />
<h2 id="what"><a class="toclink" href="#what">What</a></h2>
<p>Client Hello Mirror is a server which outputs your browser's TLS Client Hello message, emphasizing aspects of it that are detrimental to privacy:</p>
<p>» <a href="https://tlsprivacy.nervuri.net/">tlsprivacy.nervuri.net</a></p>
<p>It supports HTTPS and Gemini, is written in Go and is free/libre software.  I'll explain why and how I wrote it.</p>
<h2 id="why"><a class="toclink" href="#why">Why</a></h2>
<p>A TLS connection starts with the client sending the server a "Hello" message that contains a set of supported capabilities and various parameters.  This initial message presents two main privacy problems:</p>
<ul>
<li>it often includes a unique session resumption token which can be used to track visitors (this is a problem on the web, less so in Geminispace);</li>
<li>by way of fingerprinting, it reveals to servers and on-path observers what browser you are likely using (down to its version, or a range of versions); if you change any TLS-related settings, your TLS fingerprint becomes specific to a much smaller group of users, possibly even to you alone.</li>
</ul>
<p>» <a href="https://svs.informatik.uni-hamburg.de/publications/2018/2018-12-06-Sy-ACSAC-Tracking_Users_across_the_Web_via_TLS_Session_Resumption.pdf">Tracking Users across the Web via TLS Session Resumption (2018)</a><br />
» <a href="https://tlsfingerprint.io/">tlsfingerprint.io</a></p>
<p>I think these issues deserve more attention than they receive.</p>
<p>Among the browser testing tools available online, I hoped to find a web service that presented the <em>complete</em> Client Hello message, but my search came up empty.  So, because I wanted such a tool to exist (for both Gemini and the web) and because I wanted to draw more attention to TLS privacy issues, I wrote Client Hello Mirror.  This was a bit of a challenge, since many of the values in the Client Hello message are not exposed by TLS libraries.</p>
<h2 id="how"><a class="toclink" href="#how">How</a></h2>
<h3 id="first-steps"><a class="toclink" href="#first-steps">First steps</a></h3>
<p>I chose Go for this project for a single reason: uTLS, a "fork of the Go standard TLS library, providing low-level access to the ClientHello for mimicry purposes", made by the folks behind tlsfingerprint.io.  Go developers are spoiled with such libraries: see also JA3Transport and CycleTLS, both of which are based on uTLS.</p>
<ul>
<li style="list-style-type:'» ';"> <a href="https://github.com/refraction-networking/utls">uTLS</a></li>
<li style="list-style-type:'» ';"> <a href="https://github.com/CUCyber/ja3transport">JA3Transport</a></li>
<li style="list-style-type:'» ';"> <a href="https://github.com/Danny-Dasilva/CycleTLS">CycleTLS</a></li>
</ul>
<p>This made Go look like a good language for messing around with TLS – and indeed it is.</p>
<p>The first hurdle was figuring out how to extract the Client Hello bytes from the TCP stream and return them in an HTTP or Gemini response.  I found the answer in Filippo Valsorda's GoLab 2018 talk "Building a DIY proxy with the net package" – the <code>io.MultiReader</code> trick that he details was exactly what was needed.</p>
<p>» <a href="https://onion.tube/watch?v=J4J-A9tcjcA">Building a DIY proxy with the net package</a><br />
» <a href="https://github.com/FiloSottile/mostly-harmless/tree/main/talks/asyncnet">(code and slides)</a><br />
» <a href="https://tildegit.org/nervuri/client-hello-mirror/src/commit/89efe5d18f8e5bd330f846d0264063d6be9e747f/server.go#L70">peek function (Client Hello Mirror)</a></p>
<p>Once I had the raw Client Hello bytes, the next step was to decode them.  I based the Client Hello parser on the <code>clientHelloMsg.unmarshal</code> function in Go's built-in TLS library.  The Client Hello message breakdown in Michael Driscoll's "The Illustrated TLS 1.3 Connection" was helpful in further developing the parser, as was Wireshark.</p>
<p>» <a href="https://tls13.xargs.org/#client-hello">The Illustrated TLS 1.3 Connection</a></p>
<p>And so the first version of the tool came about, which returned the Client Hello message as JSON.</p>
<h3 id="cve-2022-30629"><a class="toclink" href="#cve-2022-30629">CVE-2022-30629</a></h3>
<p>After staring at the JSON output for a while, I noticed that <code>obfuscated_ticket_age</code> values for pre-shared keys (used for session resumption in TLS 1.3) weren't obfuscated at all.  No matter what client I used, when resuming a session, the number of milliseconds since my last connection was plainly embedded in the Client Hello message, exposed to on-path observers.  That's because Go's TLS server was setting the <code>ticket_age_add</code> value to zero for all session tickets, so clients added zero to the ticket age, resulting in no obfuscation.</p>
<p>I reported this on May 10 2022, as Go 1.19 was nearing release.  Go's security people gave this issue a CVE ID and backported the fix to Go 1.17 and 1.18 as well.</p>
<p>» <a href="https://github.com/golang/go/issues/52814">Bug report</a><br />
» <a href="https://www.opencve.io/cve/CVE-2022-30629">CVE-2022-30629</a></p>
<blockquote>
<p>Non-random values for <code>ticket_age_add</code> in session tickets in crypto/tls before Go 1.17.11 and Go 1.18.3 allow an attacker that can observe TLS handshakes to correlate successive connections by comparing ticket ages during session resumption.</p>
</blockquote>
<h3 id="privileges-and-timeouts"><a class="toclink" href="#privileges-and-timeouts">Privileges and timeouts</a></h3>
<p>Part of making this server was figuring out how to properly drop root privileges in Go and how to correctly set timeouts on TCP connections.  Tackling these issues is not as straightforward as it may appear.  I assisted Solderpunk in dealing with them for Molly Brown as well.</p>
<p>» <a href="https://stackoverflow.com/a/75545491">Golang: dropping privileges – my Stack Overflow answer</a><br />
» <a href="https://tildegit.org/solderpunk/molly-brown/issues/16#issuecomment-10573">Molly Brown: drop privileges</a><br />
» <a href="https://tildegit.org/solderpunk/molly-brown/issues/35">Molly Brown: timeouts</a></p>
<p>The timeouts thread goes into tedious subtleties regarding what really happens when you call <code>Close()</code> on a TCP connection.  It turns out that, by default, the kernel doesn't close the connection until its write buffer is emptied.  The write buffer can be quite large and connections can be quite slow, so this can take a very long time – hours/days <em>after</em> you call <code>Close()</code>.  So if you're looking to make it harder for "slow loris" attacks to exhaust socket descriptors, don't rely on timeouts/deadlines without also calling <code>SetLinger(0)</code> on the TCP connection before closing it.</p>
<p>» <a href="https://pkg.go.dev/net#TCPConn.SetLinger">TCPConn.SetLinger</a></p>
<h3 id="tlshelloagwaname"><a class="toclink" href="#tlshelloagwaname">tlshello.agwa.name</a></h3>
<p>About one year after starting this project, I came across a blog post by Andrew Ayer titled "Parsing a TLS Client Hello with Go's cryptobyte Package".  It turns out that he wrote a very similar server at about the same time as me:</p>
<p>» <a href="https://tlshello.agwa.name/">tlshello.agwa.name</a><br />
» <a href="https://www.agwa.name/blog/post/parsing_tls_client_hello_with_cryptobyte">Parsing a TLS Client Hello with Go's cryptobyte Package</a><br />
» <a href="https://github.com/AGWA/tlshacks">github.com/AGWA/tlshacks</a></p>
<p>Internally, his approach is very different.  For one thing, he wrote the code for parsing the Client Hello message from scratch, whereas I extended the parser in Go's TLS library.  For another thing, he managed to expose the full Client Hello message to a standard Go HTTP listener, which is something I had failed to figure out, leading me to do HTTP "by hand".</p>
<p>Not using a proper HTTP library may sound like asking for trouble on the request parsing side, but my code only looks at the first line of the request.  It's so trivial that I dare say it is secure, as it only deals with the minimal subset of HTTP required for this to work (no request headers, no methods other than GET and HEAD, no HTTP/2…) and it doesn't serve files.  Still, I would have preferred to use Go's HTTP library instead, because that would have made my code more useful to other developers.  If you need to use the Client Hello message in an HTTP response, you're probably better off using Andrew Ayer's method.</p>
<p>What I took from his implementation was the idea of extracting TLS parameter and extension information from CSV files published by IANA.  That's how the <code>/json/v2</code> endpoint was born, which expands many numeric identifiers (of TLS versions, cipher suites, etc) into JSON objects containing a bit more information.  This information is also used when generating the front page.</p>
<p>A subtle point about tlshello.agwa.name is that it doesn't use session resumption, so clients will never send it a <code>pre_shared_key</code> extension or a <code>session_ticket</code> value.</p>
<h3 id="nja3"><a class="toclink" href="#nja3">NJA3</a></h3>
<p>I wanted to highlight TLS fingerprinting, so I included the popular JA3 fingerprint in the output.  However, Chromium developers recently decided to randomize the ordering of extensions on each TLS handshake, as a counter to protocol ossification.  This makes Chromium's JA3 fingerprint change on every connection, which prompted me to make a variant of JA3 that remains the same when extensions are shuffled.  So I took JA3, sorted the extension codes and called the new fingerprint Normalized JA3 (NJA3).</p>
<p>A few days later, I came across a presentation by Troy Kent titled "(JA) 3 Reasons to Rethink Your Encrypted Traffic Analysis Strategies", which made a number of insightful suggestions, some of which I implemented.  One of them was to ignore SNI, padding and other extensions that clients don't necessarily send on every connection.  I also added five extra code groups and made a couple of changes inspired by mercury's Network Protocol Fingerprinting (NPF) specification.  These modifications made NJA3 more precise and robust.  It's more than "Normalized JA3" at this point.</p>
<p>» <a href="https://tildegit.org/nervuri/client-hello-mirror/src/branch/master/NJA3.md">NJA3 documentation</a><br />
» <a href="https://www.fastly.com/blog/a-first-look-at-chromes-tls-clienthello-permutation-in-the-wild">A first look at Chrome's TLS ClientHello permutation in the wild</a><br />
» <a href="https://onion.tube/watch?v=C93ivdcVL3A">"(JA) 3 Reasons to Rethink Your Encrypted Traffic Analysis Strategies"</a><br />
» <a href="https://github.com/cisco/mercury/blob/main/doc/npf.md">Network Protocol Fingerprinting (NPF) specification</a></p>
<h2 id="mentions"><a class="toclink" href="#mentions">Mentions</a></h2>
<ul>
<li>If you connect to tlsprivacy.nervuri.net using Firefox / Tor Browser, you'll get a warning that your browser doesn't validate Signed Certificate Timestamps (for Certificate Transparency).  You can enable SCT support by setting <code>security.pki.certificate_transparency.mode = 1</code> in about:config, but doing so makes your TLS fingerprint stand out.  Mozilla should enable this by default.</li>
<li>tlsfingerprint.io has a JSON endpoint which returns detailed (but not complete) information extracted from the Client Hello message.  BrowserLeaks also presents such information in its TLS section:</li>
</ul>
<p>» <a href="https://client.tlsfingerprint.io/">client.tlsfingerprint.io</a><br />
» <a href="https://browserleaks.com/tls">browserleaks.com/tls</a></p>
<h2 id="closing-thoughts"><a class="toclink" href="#closing-thoughts">Closing thoughts</a></h2>
<p>Some of the features that I wished for didn't make it in.  I would have liked the server to support early data / 0-RTT session resumption, as well as the legacy sessionID-based resumption method, but Go's <code>crypto/tls</code> library does not support them.</p>
<p>Also, I would have liked the server to detect clients' susceptibility to session prolongation attacks (see section 3.1 of the paper linked below).  That, however, would require substantially more effort than it's probably worth.  What's important is to know that even though the maximum lifetime of TLS 1.3 pre-shared keys is 7 days, a server can use them to track visitors over a much longer period, by just issuing a new one on each connection.  This allows for tracking users indefinitely, as long as they connect at least once a week.  This can be solved by clients sticking to the expiry date of the initial pre-shared key, but I doubt that any TLS libraries do this.  As for other resumption methods, TLS session tickets and session IDs have a shorter maximum lifetime, but otherwise have the same problem.</p>
<p>» <a href="https://svs.informatik.uni-hamburg.de/publications/2018/2018-12-06-Sy-ACSAC-Tracking_Users_across_the_Web_via_TLS_Session_Resumption.pdf#page=3">Tracking Users across the Web via TLS Session Resumption (2018)</a></p>
<p>TLS token binding (RFCs 8471, 8472 and 8473, formerly Channel ID) looks like it can be as bad for privacy as session resumption, but Chromium removed support for it in 2018.  Edge might still support it, though.  Token binding appears to be on its way out, but if it sticks around, Client Hello Mirror will probably highlight it at some point.</p>
<p>This concludes my exploration of TLS privacy issues, at least for now.  On a similar note, I'm also interested in figuring out how feasible it is nowadays to determine device clock skews via the TCP timestamps option, and to what extent they can be used for device fingerprinting.  But I'll leave that for another time.</p>
<p>» <a href="https://homes.cs.washington.edu/~yoshi/papers/PDF/KoBrCl05PDF-lowres.pdf">Remote physical device fingerprinting (2005)</a></p>
</article>
</main>

<hr/>
<footer class="bottom">
Published: <time datetime="2023-10-13">2023-10-13</time><br/>
Updated: <time datetime="2023-11-01">2023-11-01</time><br/>
<a href="https://gitlab.com/nervuri/nervuri.net/-/blob/master/markdown/client-hello-mirror.md">Source</a> (contributions welcome)<br/>
License: <a rel="license" href="https://www.tldrlegal.com/license/bsd-3-clause-license-revised">BSD 3-Clause</a>
</footer>

</body>
</html>
