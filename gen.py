#!/usr/bin/env python3

from markdown import Markdown
from md2gemini import md2gemini
from html2text import HTML2Text
from string import Template
from subprocess import call
from pathlib import Path
from bs4 import BeautifulSoup
# import re
import regex as re
from urllib.parse import urljoin
import sys
import os

base_urls = {
    'web': 'https://nervuri.net/',
    'gemini': 'gemini://rawtext.club/~nervuri/',
    'gopher': 'gopher://rawtext.club/0/~nervuri/',
    'source': 'https://gitlab.com/nervuri/nervuri.net/-/blob/master/markdown/',
}

licenses = {
    'CC-BY-SA 4.0': 'https://creativecommons.org/licenses/by-sa/4.0/',
    'BSD 3-Clause': 'https://www.tldrlegal.com/license/bsd-3-clause-license-revised',
    'MPL 2.0': 'https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2)',
    'GFDL 1.3-or-later': 'https://www.gnu.org/licenses/fdl-1.3.txt',
}

feed = {
    'updated': '',
    'author': 'nervuri',
    'rights': 'CC-BY-SA 4.0 unless otherwise specified - https://creativecommons.org/licenses/by-sa/4.0/',
    'entries': [],
}

def makepage(filepath):

    # prune the path:
    # markdown/path/file.md -> path/file
    path, ext = os.path.splitext(filepath)
    pruned_path = path[path.find('/')+1:]

    # make canonical link
    if pruned_path.endswith('/index') or pruned_path == 'index':
        canonical_link = base_urls['web'] + pruned_path[:-5]
    else:
        canonical_link = base_urls['web'] + pruned_path

    main = ''
    footer = ''
    metadata = {
        'title': 'nervuri',
        'subtitle': '',
        'title_image': '',
        'table_of_contents': 'false',
        'article': 'true',
        'web': canonical_link,
        'source': base_urls['source'] + pruned_path + '.md',
        'also_on': '',
    }

    with open(filepath, 'r') as current_file:
        # read the first part of the file as metadata
        for line in current_file:
            if 'metadata' in line:  # first line
                continue
            elif line == '```\n':  # last line
                break
            else:
                metadata[line.split(': ')[0]] = ': '.join(
                            line.split(': ')[1:]
                        ).rstrip()
        if ext == '.md':
            # read the rest of the file as markdown
            md_config = {
                'toc': { 'anchorlink': True },
                'smarty': {  # https://python-markdown.github.io/extensions/smarty/
                    'smart_dashes': True,
                    'smart_quotes': False,
                    'smart_angled_quotes': True,
                    'smart_ellipses': True,
                    'substitutions': {
                        'ndash': '–',  # U+2013
                        'mdash': '—',  # U+2014
                        'left-angle-quote': '«',
                        'right-angle-quote': '»',
                        'ellipsis': '…',
                    }
                }
            }
            md = Markdown(
                    extensions = ['toc', 'fenced_code', 'smarty'],
                    extension_configs = md_config)
            toc = ''
            if metadata['table_of_contents'] == 'true':
                toc = '\n\n---\n\n[TOC]\n\n---\n\n'
            body = current_file.read()
            main = md.convert(toc + body)
        elif ext == '.gmi':
            exit('TODO: .gmi not yet supported')
            # add the title and subtitle as gemtext
            # copy the plain gemtext to gemini/ (and add journal entry)
            # make HTML from gemtext

    if 'gemini' in metadata['also_on']:
        if pruned_path == 'index':
            metadata['gemini'] = base_urls['gemini']
        else:
            metadata['gemini'] = base_urls['gemini'] + pruned_path + '.gmi'
    if 'gopher' in metadata['also_on']:
        if pruned_path == 'index':
            metadata['gopher'] = base_urls['gopher'].replace('/0/', '/1/')
        else:
            metadata['gopher'] = base_urls['gopher'] + pruned_path + '.txt'

    # generate article HTML (and, optionally, gemtext and plain text)
    if metadata['article'] == 'true':

        # Markdown->HTML conversion removes two characters from the
        # zero-width test, so we need to add them here:
        main = main.replace('02: __ start of text', '02: __ start of text')
        main = main.replace('03: __ end of text', '03: __ end of text')

        if 'gemini' in metadata['also_on'] and pruned_path != 'zero-width':
            # Convert article to gemtext.
            # Don't convert the zero-width characters article,
            # because some chars get lost in conversion.
            gemtext = '# ' + metadata['title'] + '\n\n'
            if metadata['subtitle'] != '':
                gemtext += '## ' + metadata['subtitle'] + '\n\n'
            gemtext += md2gemini(body, strip_html=True).replace(
                    '\r\n', '\n'    # dos2unix (line endings)
                ).replace(
                    '* \n=>', '=>'  # lists of links
                )
            # replace '---' with horizontal line
            gemtext = re.sub(r'^(-)\1{3,}$',
                    '_____________________', gemtext, flags=re.MULTILINE)
            if pruned_path == 'client-hello-mirror':
                gemtext = gemtext.replace(
                    'https://tlsprivacy.nervuri.net',
                    'gemini://tlsprivacy.nervuri.net')
            # add footer
            gemtext += '\n\n_____________________\n\n'
            if 'published' in metadata:
                gemtext += 'Published: {0}\n'.format(metadata['published'])
            if 'updated' in metadata and metadata['updated'] != metadata['published']:
                gemtext += 'Updated:   {0}\n'.format(metadata['updated'])
            if 'source' in metadata:
                gemtext += '=> {0}  Source (contributions welcome)\n'.format(metadata['source'])
            if 'license' in metadata:
                gemtext += '=> {0}  License: {1}\n'\
                    .format(licenses[metadata['license']], metadata['license'])
            # write to file
            with open('gemini/' + pruned_path + '.gmi', 'w') as f:
                f.write(gemtext)
            print('gemini/' + pruned_path + '.gmi')

            # HTML: mark the start of links.
            main = main.replace('<a href', '» <a href')
            # In lists of links, change the bullet types.
            main = main.replace( 'li>» <a href',
                    'li style="list-style-type:\'» \';"> <a href')

        # generate HTML
        title_section = '<h1>' + metadata['title'] + '</h1>\n'
        if metadata['subtitle'] != '' or metadata['title_image'] != '':
            if metadata['subtitle'] != '':
                metadata['subtitle'] = '\n\t<i>' + metadata['subtitle'] + '</i>'
            if metadata['title_image'] != '':
                metadata['title_image'] = '\n\t' + metadata['title_image']
            title_section = '''<div class="center">{title_image}
\t<h1>{title}</h1>{subtitle}\n</div>\n'''.format(**metadata)
        main = '<article>\n' + title_section + main + '\n</article>'
        # add footer
        footer = '\n<hr/>\n'
        footer += '<footer class="bottom">\n'
        if 'published' in metadata:
            footer += 'Published: <time datetime="{0}">{0}</time><br/>\n'.format(metadata['published'])
        if 'updated' in metadata and metadata['updated'] != metadata['published']:
            footer += 'Updated: <time datetime="{0}">{0}</time><br/>\n'.format(metadata['updated'])
        if 'source' in metadata:
            footer += '<a href="{0}">Source</a> (contributions welcome)<br/>\n'.format(metadata['source'])
        if 'license' in metadata:
            if metadata['license'] in licenses:
                footer += 'License: <a rel="license" href="' + licenses[metadata['license']] + '">' + metadata['license'] + '</a>\n'
            else:
                footer += 'License: ' + metadata['license'] + '\n'
        footer += '</footer>\n'

        if 'gopher' in metadata['also_on'] and pruned_path != 'zero-width':
            # Convert article to plain text.
            # Don't convert the zero-width characters article,
            # because some chars get lost in conversion.
            h = HTML2Text()
            h.unicode_snob = False
            h.ignore_links = False
            h.inline_links = False
            h.protect_links = True
            h.wrap_links = False
            h.use_automatic_links = True
            h.links_each_paragraph = True
            h.wrap_list_items = True
            text = h.handle(main)
            # Get rid of trailing whitespace and consecutive newlines.
            text = re.sub(r'[ \t]+$', '', text, flags=re.MULTILINE)
            text = re.sub(r'\n$\n$', '\n', text, flags=re.MULTILINE)
            text = re.sub(r'\n$\n$', '\n', text, flags=re.MULTILINE)
            # write to file
            with open('gopher/' + pruned_path + '.txt', 'w') as f:
                f.write(text)
            print('gopher/' + pruned_path + '.txt')

    # add to feed
    if pruned_path == 'index':  # if index page
        feed['title'] = metadata['title']
        feed['subtitle'] = metadata['description']
        feed['link_alt'] = { 'text/html': metadata['web'] }
        # Feedreaders behave inconsistently when multiple alternate
        # links are present, even though the Atom spec allows it.
        #if 'gemini' in metadata:
        #    feed['link_alt']['text/gemini'] = metadata['gemini']
        #if 'gopher' in metadata:
        #    feed['link_alt']['text/plain'] = metadata['gopher']
        feed['link_self'] = metadata['web'] + 'feed.atom'
        feed['icon'] = metadata['web'] + 'favicon.ico'
        feed['id'] = metadata['web']
    elif metadata['article'] == 'true':
        entry = {}
        entry['title'] = metadata['title']
        entry['link_alt'] = { 'text/html': metadata['web'] }
        # Feedreaders behave inconsistently when multiple alternate
        # links are present, even though the Atom spec allows it.
        #if 'gemini' in metadata:
        #    entry['link_alt']['text/gemini'] = metadata['gemini']
        #if 'gopher' in metadata:
        #    entry['link_alt']['text/plain'] = metadata['gopher']
        entry['id'] = metadata['web']
        entry['published'] = metadata['published']
        entry['updated'] = metadata['updated'] if 'updated' in metadata \
                else metadata['published']
        if metadata['license'] in licenses:
            entry['rights'] = metadata['license'] + ' - ' + licenses[metadata['license']]
        else:
            entry['rights'] = metadata['license']
        entry['summary'] = metadata['description']

        ## For Atom feed content, convert relative links to absolute links. ##
        content = BeautifulSoup(main + footer, 'html.parser')
        # Get relative links (links which don't start with `#` and don't contain `:`).
        relative_links = content.find_all('a', href=re.compile('^[^#](?!.*:)'))
        for link in relative_links:
            link['href'] = urljoin(base_urls['web'] + pruned_path, link['href'])
        # Get rid of feed-breaking character from the zero-width character test
        if entry['id'] == 'https://nervuri.net/zero-width':
            content = str(content)
            removed_msg = ' (removed from Atom feed, as this character is not allowed in XML)'
            # ASCII control codes
            content = re.sub(r'_\p{Cc}_.*', '__' + removed_msg, content, flags=re.MULTILINE)
            # Other invalid characters
            content = re.sub('FFFE: _._.*',
                    'FFFE: __ <not a character>' + removed_msg, content, flags=re.MULTILINE)
            content = re.sub('FFFF: _._.*',
                    'FFFF: __ <not a character>' + removed_msg, content, flags=re.MULTILINE)
        entry['content'] = content
        feed['entries'].append(entry)

    with open('templates/main.tpl', 'r') as f:
        main_template = Template(f.read())

    extra_tags = ''
    if 'description' in metadata:
        extra_tags += '<meta name="description" content="' + metadata['description'] + '"/>\n'
    if 'web' in metadata:
        extra_tags += '<link rel="canonical" href="' + metadata['web'] + '"/>\n'

    path = ''
    for i in range(pruned_path.count('/')):
        path += '../'

    top = ''
    if pruned_path != 'index':  # if not index page
        metadata['title'] += ' | nervuri'
        # add top section
        other_protocols = ''
        if 'gemini' in metadata:
            other_protocols += '<a href="' + metadata['gemini'] + '">Gemini</a> |\n		'
        if 'gopher' in metadata:
            other_protocols += '<a href="' + metadata['gopher'] + '">Gopher</a> |\n		'
        with open('templates/top.tpl', 'r') as f:
            top_template = Template(f.read())
        top = top_template.substitute(
            OTHER_PROTOCOLS = other_protocols,
            PATH = path,
        )

    page_html = main_template.substitute(
        PATH = path,
        TITLE = metadata['title'],
        EXTRA_TAGS = extra_tags,
        MAIN = main,
        FOOTER = footer,
        TOP = top,
    )

    with open('public/' + pruned_path + '.html', 'w') as f:
        f.write(page_html)


############################################################

### make html files ###

page_to_open = ''

# If path to a .md or .gmi file is provided, make only that page.
# Else, make all pages + Atom feed.
if len(sys.argv) > 1:
    filepath = sys.argv[1]
    if filepath[:9] != 'markdown/':
        quit('path must begin with "markdown/"')
    if filepath[-3:] != '.md' and filepath[-4:] != '.gmi':
        quit('file extension must be ".md" or ".gmi"')
    if not Path(filepath).is_file():
        quit('file does not exist')
    makepage(filepath)
    page_to_open = filepath
else:
    max_mtime = 0
    for filepath in Path('markdown').rglob('*.[mg][dm]*'):  # match .md and .gmi
        if filepath.stat().st_mtime > max_mtime:
            max_mtime = filepath.stat().st_mtime
            page_to_open = str(filepath)
        makepage(filepath)

    ### make feed ###

    # sort entries by 'updated'
    feed['entries'] = sorted(feed['entries'], key=lambda k: k['updated'], reverse=True)
    # feed['updated'] -> most recent updated entry
    feed['updated'] = feed['entries'][0]['updated']

    with open('templates/feed.tpl', 'r') as f:
        feed_template = Template(f.read())

    with open('templates/feed-entry.tpl', 'r') as f:
        feed_entry_template = Template(f.read())

    entries = ''
    for entry in feed['entries']:
        link_alt = ''
        for mimetype, link in entry['link_alt'].items():
            link_alt += f'    <link type="{mimetype}" href="{link}"/>\n'
        entries += feed_entry_template.substitute(
            TITLE = entry['title'],
            ID = entry['id'],
            LINK_ALT = link_alt,
            PUBLISHED = entry['published'],
            UPDATED = entry['updated'],
            SUMMARY = entry['summary'],
            CONTENT = entry['content'],
            RIGHTS = entry['rights'],
        )

    link_alt = ''
    for mimetype, link in feed['link_alt'].items():
      link_alt += f'  <link rel="alternate" type="{mimetype}" href="{link}"/>\n'

    feed_xml = feed_template.substitute(
        TITLE = feed['title'],
        SUBTITLE = feed['subtitle'],
        ID = feed['id'],
        LINK_ALT = link_alt,
        LINK_SELF = feed['link_self'],
        ICON = feed['icon'],
        UPDATED = feed['updated'],
        AUTHOR = feed['author'],
        RIGHTS = feed['rights'],
        ENTRIES = entries,
    )

    with open('public/feed.atom', 'w') as f:
        f.write(feed_xml)

    ### sign website ###

    #call(['netsigil', '--sign', 'public/'])

############################################################

### open last modified page in browser ###

# First prune the path:
# markdown/path/file.md -> path/file
path, ext = os.path.splitext(page_to_open)
pruned_path = path[path.find('/')+1:]
# call(['xdg-open', 'public/' + pruned_path + '.html'])
