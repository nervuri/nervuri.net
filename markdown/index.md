``` metadata
title: nervuri
article: false
description: “To create a little flower is the labour of ages” (William Blake)
also_on: gemini, gopher
```

<div class="index">
<div class="center">
<figure lang="ro">
	<img src="img/nervuri.jpg" alt="frunză penatinervă" title="frunză penatinervă" width="600" height="450"/><br/>
	<figcaption>Din lumină foșnet soarbe</figcaption>
</figure>
<div>
	<a href="contact">Contact</a> | <a href="feed.atom" title="subscribe">RSS</a>
</div>
</div>

<ul>
<li><span class="date">2023-10-13</span> » <a href="client-hello-mirror">TLS Client Hello Mirror</a></li>
<li><span class="date">2023-08-11</span> » <a href="email/privacy">Notes on e-mail privacy</a></li>
<li><span class="date">2022-10-09</span> » <a href="e/app-lounge"><span class="nowrap">Analysis of the /e/OS app installer,</span> <span class="nowrap">part 2</span></a></li>
<li><span class="date">2021-10-29</span> » <a href="e/apps"><span class="nowrap">Analysis of the /e/OS app installer,</span> <span class="nowrap">part 1</span></a></li>
<li><span class="date">2021-02-20</span> » <a href="stega" class="nowrap">Tracking via pasted text</a></li>
<li><span class="date">2021-02-20</span> » <a href="zero-width" class="nowrap">Zero-width character test</a></li>
</ul>

</div>
