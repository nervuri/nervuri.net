``` metadata
title: contact
article: false
description: contact nervuri
```

<div class="center">
	<h1>contact</h1>
	<p>E-mail: <b>seva</b> <em>at</em> <b>nervuri</b> <em>dot</em> <b>net</b></p>
	<p><a href="keys/">PGP key</a></p>
</div>

---

* [Gemini capsule](gemini://rawtext.club/~nervuri/)

Code repositories:

* [tildegit.org/nervuri](https://tildegit.org/nervuri)
* [gitlab.com/nervuri](https://gitlab.com/nervuri)
* [github.com/nervuri](https://github.com/nervuri)
