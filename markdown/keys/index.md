``` metadata
title: keys
article: false
description: PGP and signify keys
```

# keys

PGP key for <**seva** _at_ **nervuri** _dot_ **net**\>:  
[B769BD004A417E3A5A902DD1C4769EEA7BA61672](openpgp4fpr:B769BD004A417E3A5A902DD1C4769EEA7BA61672)

It is available in multiple locations:

* [this website](nervuri-pgp.asc)
* [GitLab](https://gitlab.com/nervuri/nervuri.net/-/raw/master/public/keys/nervuri-pgp.asc)
* [Keybase](https://keybase.io/nervuri)
* [keys.openpgp.org](https://keys.openpgp.org/vks/v1/by-fingerprint/B769BD004A417E3A5A902DD1C4769EEA7BA61672) ([Tor onion service](http://zkaan2xfbuxia2wpf7ofnkbz6r5zdbbvxbunvp5g2iebopbfc4iqmbad.onion/vks/v1/by-fingerprint/B769BD004A417E3A5A902DD1C4769EEA7BA61672))

<!-- TODO: uncomment this after improving NetSigil
There is also a [signify](https://www.openbsd.org/papers/bsdcan-signify.html) key, used to sign this website via [NetSigil](https://tildegit.org/nervuri/NetSigil):  
[nervuri-signify.pub](nervuri-signify.pub)  
It is signed by the PGP key:  
[nervuri-signify.pub.sig](nervuri-signify.pub.sig)
-->
