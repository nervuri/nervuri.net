``` metadata
title: Analysis of the /e/OS app installer, part 2
table_of_contents: true
description: Issues with App Lounge, the new /e/OS app installer
published: 2022-10-09
updated: 2023-03-29
license: CC-BY-SA 4.0
```

In [the first part](apps) of this analysis (October 2021), I detailed a number of security and privacy problems with the [/e/OS](https://e.foundation/) app installer.  To recap:

> ["Apps"](https://gitlab.e.foundation/e/apps/apps), the /e/ app installer, downloads applications from [CleanAPK.org](https://info.cleanapk.org/), an intermediary which provides apps that originate from [F-Droid](https://f-droid.org/) and [elsewhere](apps#app-sources).
>
> Since apps are not downloaded directly from F-Droid or Google Play, the installer takes certain [measures](https://info.cleanapk.org/) to protect against tampering.  Unfortunately, these measures can be bypassed in the majority of cases.  This means that CleanAPK.org (or whoever compromises it) can get maliciously modified apps installed on /e/ users' devices, either when the user is installing a new app or during the update process.
>
> Such an attack can be targeted at specific users, based on device information which the installer reveals to the CleanAPK server every time it checks for updates, namely: the list of installed apps, device model, build ID, Android version and installed languages.

Since then, things have changed somewhat.  May 2022 brought the launch of /e/OS v1.0, as well as [App Lounge](https://community.e.foundation/t/app-lounge-know-all-about-it/42331), a rewrite of the installer which downloads applications directly from Google Play.

## Signature verification

I expected this release to solve all security vulnerabilities detailed in the previous article, but it wasn't until [/e/OS v1.3](https://gitlab.e.foundation/e/os/releases/-/releases#please-welcome-eos-13-rocket) (App Lounge v2.3.4) that [they got dealt with](https://gitlab.e.foundation/e/os/apps/-/merge_requests/177).  The first few versions of App Lounge had F-Droid signature verification removed, which made the situation worse than before.  CleanAPK could have gotten the user to install a malicious version of *any* app just by including it in an API response (and still can, for those who haven't updated).

While understated by the developers, solving this problem was likely the most important security enhancement that the app installer ever received, so be sure to update to /e/OS 1.3 or later (App Lounge currently does not have a self-update mechanism; it is only updated along with the entire operating system).

So progress has been made.  I have not tested the patch, but it looks like, four years after the initial release, the app installer finally provides adequate protection against application spoofing.  This covers native apps, which are in the majority.  PWAs, however, are a separate discussion.

## Progressive web apps

Alongside native apps from Google Play and F-Droid, the app installer also provides [progressive web apps (PWAs)](https://en.wikipedia.org/wiki/Progressive_web_app): web pages which can be installed alongside native applications.  CleanAPK provides [a list of links](https://api.cleanapk.org/v2/apps?action=list_apps&type=pwa) to PWAs from all over the web.  When you install a PWA, App Lounge does nothing to verify that the link pointing to it is correct.  CleanAPK can easily change any of these links and serve spoofed PWAs.

Responding to this observation, one of the developers mentioned that /e/ will be looking into [W3C MiniApps](https://www.w3.org/TR/miniapp-packaging/#sec-miniapp-processing), which can be packaged and verified.  I don't know what their plans for MiniApps are, but as long as App Lounge provides PWAs, something ought to be done to secure their delivery.

My suggestion to /e/ devs is to make App Lounge update its PWA index by fetching a verified, offline-signed list from an /e/ server.  Note, however, that this would only protect the list of links.  It would not help against PWA servers themselves being compromised, which is something that web apps in general are not protected against.  [Web apps *can* be signed](https://github.com/tasn/webext-signed-pages), but hardly ever are, because browsers have no built-in way to verify such signatures.

I also suggest more prominently presenting the PWA's URL within App Lounge, so that users have a better chance to spot at least obvious alterations, such as "uber.com" being changed to "evil.xxx".  App Lounge currently displays each PWA's domain name under the "package name" field at the bottom of the app details page, which is easy to miss.

A quirk of CleanAPK is that all PWA links that it provides end with the redundant parameters `?force=pwa&source=mlpwa`.  The `source` parameter looks like it may be a referrer signal, but a developer told me that this is not intended nor required and asked me to open a ticket for it.  Here is that ticket: [https://gitlab.e.foundation/e/backlog/-/issues/5767](https://gitlab.e.foundation/e/backlog/-/issues/5767).  I opened it three months ago, no change yet.

Side note: around March 2022, /e/ devs [were planning](https://community.e.foundation/t/week-13-2022-development-and-testing-updates/39929#app-lounge-9) to get PWAs from [pwastore.com](https://www.pwastore.com/) rather than CleanAPK, but they seem to have dropped that idea.

## Privacy

App Lounge inherits the previous installer's [privacy problems](apps#privacy-issues) and adds a few more.  Namely, [it sends Google](https://doc.e.foundation/calls_to_google_servers#app-lounge) the list of apps you have installed and the following device properties:

* `Build.BOOTLOADER`
* `Build.BRAND`
* `Build.DEVICE`
* `Build.FINGERPRINT`
* `Build.HARDWARE`
* `Build.ID`
* `Build.MANUFACTURER`
* `Build.MODEL`
* `Build.PRODUCT`
* `Build.RADIO`
* `Build.VERSION.RELEASE`
* `Build.VERSION.SDK_INT`
* `Features`
* `GL.Extensions`
* `GL.Version`
* `GSF.version`
* `HasFiveWayNavigation`
* `HasHardKeyboard`
* `Keyboard`
* `Locales`
* `Navigation`
* `Platforms`
* `Screen.Density`
* `Screen.Height`
* `Screen.Width`
* `ScreenLayout`
* `SharedLibraries`
* `TouchScreen`
* `UserReadableName`
* `Vending.versionString`
* `Vending.version`

This info is sent regardless of whether the user logs in with a Google account.  An example of the values these properties can take is available [here](https://gitlab.e.foundation/e/documentation/user/-/merge_requests/651#note_218593).  The corresponding source code can be found [here](https://gitlab.e.foundation/e/os/apps/-/blob/89a9d4b41136a5c3d3fd400b1554ab2ea5e9fd18/app/src/main/java/foundation/e/apps/api/gplay/utils/NativeDeviceInfoProviderModule.kt#L42).

It appears that sending this data is at least partly necessary in order to get apps from Google.  Aurora Store behaves similarly, although [its privacy policy](https://gitlab.com/AuroraOSS/AuroraStore/-/blob/master/POLICY.md) does not go into much detail (I opened [an issue](https://gitlab.com/AuroraOSS/AuroraStore/-/issues/841) about this).  However, unlike App Lounge, Aurora Store provides [a way to spoof](https://web.archive.org/web/20220111161806/https://auroraoss.com/faq/#spoofing) device information it sends to Google.

I want to stress that the only way to get apps from Google Play without having to trust a third party is to download them straight from Google, which implies disclosing (at least some of) this data.  I don't fault /e/ for using this method to gain access to Google Play apps.  However, the user should be made aware of its privacy implications and be allowed to opt out before any connection is made to Google.

**UPDATE 2023-02-15: [Starting with of /e/OS 1.8](https://e.foundation/leaving-apple-google-welcome-e-os-1-8-do-not-miss-a-special-offer-for-murena-fairphone-4/), App Lounge has a "no-Google mode", allowing users to opt out.**

As for HTTP headers, the User-Agent header [is no longer as revealing](https://gitlab.e.foundation/search?group_id=9&project_id=355&scope=merge_requests&search=agent) as it was, but this doesn't help against Google, because the device details listed above are a *large* superset of the info that used to be exposed in the User-Agent string.  The list of languages installed on your system is still exposed via the Accept-Language header, which is apparently required in order to get applications in the correct language.

Data sent to Google is only documented in [an obscure page](https://doc.e.foundation/calls_to_google_servers#app-lounge) on the e.foundation website, which is better than nothing -- in today's world it even stands out as going above and beyond.  But privacy-related information should be presented clearly to all users before any app sends any data to any server.  Does App Lounge contain an intro screen telling people what data goes where?  No.  The intro screen contains [a block of legalese](https://gitlab.e.foundation/e/os/apps/-/blob/1d3e42849149fa2aba49c491a6b2e1433455d9cf/app/src/main/res/raw/terms_of_use.html) that does not mention any of this.

In conclusion, App Lounge sends Google more than enough information to identify most devices, even in its so-called "anonymous mode".  /e/developers should look into spoofing as much of this info as possible.  Regarding the list of installed applications, one way to hide it from Google might be for App Lounge to imitate [Parcimonie](https://github.com/EtiennePerot/parcimonie.sh) and do update checks for each app separately, through a proxy (Tor, if possible), at random intervals.

## Promoting spyware

When opening App Lounge, the user is greeted with a list of mostly proprietary applications with attractive images, but full of trackers.  App Lounge fetches this list from Google Play.  Allowing Google to determine what gets promoted in /e/'s *privacy-focused* app store is obviously problematic, as it steers users in the wrong direction.  A privacy-focused system encouraging users to give up their privacy.

The old app installer has pretty much the same problem, except that it gets its app list from CleanAPK, which makes different recommendations based on unknown criteria.

In October 2021, I wrote [a forum post](https://community.e.foundation/t/an-analysis-of-the-e-os-app-installer/35802/13) and [a ticket](https://gitlab.e.foundation/e/backlog/-/issues/4143) suggesting that the app installer's homepage ought to contain entirely privacy-respecting [free/libre software](https://www.gnu.org/philosophy/free-sw.en.html).  Users could still install whatever they wanted, but they would have to look for the junk, not have it pushed on them.

The issue has been largely ignored until July 2022, when I prodded the devs about it over e-mail, which sparked a semi-private discussion [on the ticket thread](https://gitlab.e.foundation/e/backlog/-/issues/4143) (I can only see it while logged in).  In short, they think not having popular crap like Facebook one tap away is bad UX, too inconvenient for most people.  So they intend to leave these apps on the homepage, but are considering a few approaches to improve the situation, such as:

* excluding apps with a privacy score below 6 (if that doesn't rule out too many popular apps)
* highlighting the privacy score
* refining the privacy score
* modifying the homepage to also promote libre apps
* adding a new app category containing privacy-respecting apps recommended by /e/
* promoting libre, privacy-friendly alternatives in the app details page

Their approach could end up being better than what I initially suggested.  Having mainstream apps on the front page can be an opportunity to educate users: App Lounge could strongly highlight the problematic apps as threats to privacy and discourage their use, promoting good alternatives where available.  Will /e/ devs do this?  It remains to be seen.

The privacy score is already a step in this direction, but I would argue it is insufficient.  It is also objectively unreliable.  To give an example I used previously, Tor Browser has a lower privacy score than Facebook Lite, even though Tor Browser [does not do any tracking](https://blog.torproject.org/friends-of-tor-match-2020/#comment-290239).  Another example: if an app only requests access to contacts and to the internet, it will get a very good privacy score even if it all it does is send your contact list to a data broker.  An automatically-generated privacy score cannot be expected to be accurate in all cases and applying human review to Google Play's entire app catalog is out of the question for /e/.  Still, they could perhaps apply human review to only the most popular apps.  They could also make use of [F-Droid's tracking flag](https://f-droid.org/docs/Anti-Features/#Tracking), which [is arrived at by humans aided by various tools](https://f-droid.org/en/2020/01/16/tracking-the-trackers.html).

The /e/ team posted their current method for calculating the privacy score [in a recent developer blog post](https://community.e.foundation/t/app-lounge-know-all-about-it/42331#be-informed-regarding-privacy-4).  There they [acknowledge its limitations](https://community.e.foundation/t/app-lounge-know-all-about-it/42331#improved-privacy-score-10) and claim that they are looking into ways to improve its accuracy.

Aude M. [mentioned](https://gitlab.e.foundation/e/backlog/-/issues/4143#note_241481) requesting a study to determine the best solution for the homepage problem, but [the link to the study](https://gitlab.e.foundation/e/backlog/-/issues/5776) is dead and no information has been provided on what it entails.  All in all, I am disappointed by the lack of transparency regarding this decision.  I don't see why they didn't keep the discussion public.

## Other potential problems (untested)

1. A while ago [I did a deep dive](https://gitlab.com/fdroid/fdroidclient/-/issues/984) into potential F-Droid info leaks via TLS and HTTP.  I found that they had TLS session resumption enabled, although they thought they had disabled it.  This is a problem because TLS session identifiers can uniquely tag a device much like HTTP cookies can.  F-Droid devs quickly dealt with it by reducing TLS session lifetime to 60 seconds.  /e/ developers should take this into consideration as well, not just for App Lounge, but for any of their apps which make TLS connections.

2. Untested hypothesis: even though CleanAPK no longer sits between the user and Google Play, it may be able to block updates to Google Play apps. This is because if an application is included in both CleanAPK’s and Google Play’s API responses, only the CleanAPK result is displayed to the user. CleanAPK could present a past version of a specific Google Play app to specific users in order to keep them from updating. I have not checked this; I suppose the updater logic could be different from the what-the-user-is-shown logic, so App Lounge might grab the update from Google Play anyway. /e/ devs should check this, at least if their [plan](https://community.e.foundation/t/app-lounge-know-all-about-it/42331#fetching-open-source-applications-from-f-droid-and-get-rid-of-cleanapk-7) to cut out CleanAPK completely is not close to being achieved.

## Final thoughts

App Lounge is moving in the right direction, but is currently not as private nor as secure as its alternatives: F-Droid is better at handling free software, Aurora Store is better at dealing with Google Play and PWAs are safer to install by using a web browser and checking the URL.  With more attention paid to security, App Lounge could shine as a PWA installer, but other than that, even after all the [planned improvements](https://community.e.foundation/t/app-lounge-know-all-about-it/42331#whats-next-6) are implemented, App Lounge will still be lagging behind F-Droid and Aurora.

Also, from my perspective, the past and current issues with /e/'s app installer do not inspire confidence in /e/OS as a whole, nor do some of /e/'s [other](https://community.e.foundation/t/advanced-privacy/41395/56) [failures](https://web.archive.org/web/20200618161905/https://infosec-handbook.eu/blog/e-foundation-final-look/).  For the sake of those who will end up using the system, I hope things will improve, but I've switched to GrapheneOS a while ago and have not looked back since.

The reason I dove into /e/OS in the first place was that I had no better option for my previous phone: the OEM's Android build was spyware and none of my preferred Android distributions had functioning builds for my device.  Were it compatible with GrapheneOS, CalyxOS, DivestOS, LineageOS or Replicant, I would have used one of them instead.  At this point I wonder if I wouldn't have been better off sticking with the stock OS, [clearing out as much of the junk as possible via adb](https://inteltechniques.com/blog/2022/01/14/the-privacy-security-osint-show-episode-246/) and using something like [NetGuard](https://f-droid.org/en/packages/eu.faircode.netguard/) to block unwanted connections.  Among other advantages, going that route would have preserved [verified boot](https://source.android.com/docs/security/features/verifiedboot), a significant security feature.

The main thing that differentiates /e/ from the above-mentioned Android distros is its well-integrated suite of cloud services, offering many of the conveniences provided by a Google account, but with the same pitfall of storing your data in plaintext on someone else's computer.  If /e/ provided end-to-end encryption for those services (like [EteSync](https://www.etesync.com/) does, for instance), it would truly stand out.  Just saying "we're not Google" doesn't cut it for me.

## Discussion

This article has been discussed on [the /e/ Foundation forum](https://community.e.foundation/t/a-critical-review-of-the-app-lounge/44468).
