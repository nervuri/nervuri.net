``` metadata
title: Zero-width character test
description: Test if your software displays zero-width characters
published: 2021-02-20
updated: 2024-03-22
also_on: gemini, gopher
license: CC-BY-SA 4.0
```

Below are several zero-width Unicode characters, placed between underscores.  Can your browser display them?  Your text editor?  Your terminal?  To understand why not being able to display them might be a problem, read [Tracking via pasted text](stega) or about [the Trojan Source vulnerability](https://trojansource.codes/).

This page also contains unassigned code points and various control characters which certain programs render as zero-width even though they shouldn't.

As a point of reference, here are a few positive-width characters:

```
0020: _ _ | 00E9: _é_ | 03A9: _Ω_ | 5B57: _字_ | 1F407: _🐇_
```

## Zero-width characters

```
 00AD: _­_ soft hyphen

 034F: _͏_ combining grapheme joiner

 061C: _؜_ Arabic letter mark

 070F: _܏_ Syriac abbreviation mark

 115F: _ᅟ_ Hangul choseong filler
 1160: _ᅠ_ Hangul jungseong filler

 17B4: _឴_ Khmer vowel inherent aq
 17B5: _឵_ Khmer vowel inherent aa

 180B: _᠋_ Mongolian free variation selector one (FVS1)
 180C: _᠌_ Mongolian free variation selector two (FVS2)
 180D: _᠍_ Mongolian free variation selector three (FVS3)
 180E: _᠎_ Mongolian vowel separator
 180F: _᠏_ Mongolian free variation selector four (FVS4)

 200B: _​_ zero width space
 200C: _‌_ zero width non-joiner
 200D: _‍_ zero width joiner
 200E: _‎_ left-to-right mark
 200F: _‏_ right-to-left mark

 2028: _ _ line separator
 2029: _ _ paragraph separator

 202A: _‪_ left-to-right embedding
 202B: _‫_ right-to-left embedding
 202C: _‬_ pop directional formatting
 202D: _‭_ left-to-right override
 202E: _‮_ right-to-left override

 2060: _⁠_ word joiner
 2061: _⁡_ function application
 2062: _⁢_ invisible times
 2063: _⁣_ invisible separator
 2064: _⁤_ invisible plus
 2065: _⁥_ unassigned
 2066: _⁦_ left-to-right isolate
 2067: _⁧_ right-to-left isolate
 2068: _⁨_ first strong isolate
 2069: _⁩_ pop directional isolate
 206A: _⁪_ inhibit symmetric swapping (deprecated)
 206B: _⁫_ activate symmetric swapping (deprecated)
 206C: _⁬_ inhibit arabic form shaping (deprecated)
 206D: _⁭_ activate arabic form shaping (deprecated)
 206E: _⁮_ national digit shapes (deprecated)
 206F: _⁯_ nominal digit shapes (deprecated)

 3164: _ㅤ_ Hangul filler

 FE00: _︀_ variation selector-1 (VS1)
 FE01: _︁_ variation selector-2 (VS2)
 FE02: _︂_ variation selector-3 (VS3)
 FE03: _︃_ variation selector-4 (VS4)
 FE04: _︄_ variation selector-5 (VS5)
 FE05: _︅_ variation selector-6 (VS6)
 FE06: _︆_ variation selector-7 (VS7)
 FE07: _︇_ variation selector-8 (VS8)
 FE08: _︈_ variation selector-9 (VS9)
 FE09: _︉_ variation selector-10 (VS10)
 FE0A: _︊_ variation selector-11 (VS11)
 FE0B: _︋_ variation selector-12 (VS12)
 FE0C: _︌_ variation selector-13 (VS13)
 FE0D: _︍_ variation selector-14 (VS14)
 FE0E: _︎_ variation selector-15 (VS15)
 FE0F: _️_ variation selector-16 (VS16)

 FEFF: _﻿_ zero width no-break space

 FFA0: _ﾠ_ halfwidth Hangul filler

 FFF0: _￰_ unassigned
 FFF1: _￱_ unassigned
 FFF2: _￲_ unassigned
 FFF3: _￳_ unassigned
 FFF4: _￴_ unassigned
 FFF5: _￵_ unassigned
 FFF6: _￶_ unassigned
 FFF7: _￷_ unassigned
 FFF8: _￸_ unassigned
 FFF9: _￹_ interlinear annotation anchor
 FFFA: _￺_ interlinear annotation separator
 FFFB: _￻_ interlinear annotation terminator
 FFFC: _￼_ object replacement character

 FFFE: _￾_ <not a character>
 FFFF: _￿_ <not a character>

13430: _𓐰_ Egyptian hieroglyph vertical joiner
13431: _𓐱_ Egyptian hieroglyph horizontal joiner
13432: _𓐲_ Egyptian hieroglyph insert at top start
13433: _𓐳_ Egyptian hieroglyph insert at bottom start
13434: _𓐴_ Egyptian hieroglyph insert at top end
13435: _𓐵_ Egyptian hieroglyph insert at bottom end
13436: _𓐶_ Egyptian hieroglyph overlay middle
13437: _𓐷_ Egyptian hieroglyph begin segment
13438: _𓐸_ Egyptian hieroglyph end segment
13439: _𓐹_ Egyptian hieroglyph insert at middle
1343A: _𓐺_ Egyptian hieroglyph insert at top
1343B: _𓐻_ Egyptian hieroglyph insert at bottom
1343C: _𓐼_ Egyptian hieroglyph begin enclosure
1343D: _𓐽_ Egyptian hieroglyph end enclosure
1343E: _𓐾_ Egyptian hieroglyph begin walled enclosure
1343F: _𓐿_ Egyptian hieroglyph end walled enclosure

1BCA0: _𛲠_ shorthand format letter overlap
1BCA1: _𛲡_ shorthand format continuing overlap
1BCA2: _𛲢_ shorthand format down step
1BCA3: _𛲣_ shorthand format up step

1D159: _𝅙_ musical symbol null notehead
1D173: _𝅳_ musical symbol begin beam
1D174: _𝅴_ musical symbol end beam
1D175: _𝅵_ musical symbol begin tie
1D176: _𝅶_ musical symbol end tie
1D177: _𝅷_ musical symbol begin slur
1D178: _𝅸_ musical symbol end slur
1D179: _𝅹_ musical symbol begin phrase
1D17A: _𝅺_ musical symbol end phrase

E0000: _󠀀_ unassigned
E0001: _󠀁_ language tag (deprecated)
E0002: _󠀂_ unassigned
... (E0002-E0019 unassigned)
E0019: _󠀙_ unassigned
E0020: _󠀠_ tag space
... (E0020-E007F formerly used for tagging texts by language)
E007F: _󠁿_ cancel tag
E0080: _󠂀_ unassigned
... (E0080-E00FF unassigned)
E00FF: _󠃿_ unassigned
E0100: _󠄀_ variation selector 17
... (E0100-E01EF: variation selectors supplement)
E01EF: _󠇯_ variation selector 256
E01F0: _󠇰_ unassigned
... (E01F0-E0FFF unassigned)
E0FFF: _󠿿_ unassigned

EFFFD: _󯿽_ unassigned
EFFFE: _󯿾_ <not a character>
EFFFF: _󯿿_ <not a character>

10FFFD: _􏿽_ unassigned (private use)
10FFFE: _􏿾_ <not a character>
10FFFF: _􏿿_ <not a character>
```

Not included: samples from unassigned ranges `*FF80-*FFFF`, with the exception of `EFF80-EFFFF`, a sample of which is included.

## ASCII control codes

```
00: _ _ null
01: __ start of heading
02: __ start of text
03: __ end of text
04: __ end of transmission
05: __ enquiry
06: __ acknowledge
07: __ bell
08: __ backspace

0B: __ line tabulation (vertical tabulation)
0C: __ form feed

0E: __ shift out
0F: __ shift in
10: __ data link escape
11: __ device control one
12: __ device control two
13: __ device control three
14: __ device control four
15: __ negative acknowledge
16: __ synchronous idle
17: __ end of transmission block
18: __ cancel
19: __ end of medium
1A: __ substitute
1B: __ escape
1C: __ information separator four (file separator)
1D: __ information separator three (group separator)
1E: __ information separator two (record separator)
1F: __ information separator one (unit separator)

7F: __ delete
80: __
81: __
82: __ break permitted here
83: __ no break here
84: __ formerly known as "index"
85: __ next line
86: __ start of selected area
87: __ end of selected area
88: __ character tabulation set
89: __ character tabulation with justification
8A: __ line tabulation set
8B: __ partial line forward
8C: __ partial line backward
8D: __ reverse line feed
8E: __ single shift two
8F: __ single shift three
90: __ device control string
91: __ private use one
92: __ private use two
93: __ set transmit state
94: __ cancel character
95: __ message waiting
96: __ start of guarded area
97: __ end of guarded area
98: __ start of string
99: __
9A: __ single character introducer
9B: __ control sequence introducer
9C: __ string terminator
9D: __ operating system command
9E: __ privacy message
9F: __ application program command
```

## Thin space characters

* `0020:` \_ \_ space [for comparison]
* `205F:` \_ \_ medium mathematical space (MMSP)
* `202F:` \_ \_ narrow no-break space
* `2006:` \_ \_ six-per-em space
* `2009:` \_ \_ thin space
* `200A:` \_ \_ hair space

For more details about each character, consult the [Unicode Character Properties utility](https://util.unicode.org/UnicodeJsps/character.jsp) or the [Unicode Character Name Index](https://www.unicode.org/charts/charindex.html).

Since September 2023, Unicode is at [version 15.1](https://www.unicode.org/versions/Unicode15.1.0/) and contains 149,813 characters.

* [Unicode Character Database](https://www.unicode.org/Public/UCD/latest/)
* [Unicode 15.0 Slide Show](https://www.babelstone.co.uk/Unicode/unicode.html)

## Programs which pass the test

* [bat](https://github.com/sharkdp/bat) - when used with the "-A" option
* [cat](https://en.wikipedia.org/wiki/Cat_(Unix)) - when used with the "-v" option

## Programs whitch almost pass the test

* [Dillo](https://en.wikipedia.org/wiki/Dillo)
* [Less](https://www.greenwoodsoftware.com/less/) - when used with the "-U" option
* [Vim](https://www.vim.org/)
* [https://github.com/BurninLeo/see-non-printable-characters](https://github.com/BurninLeo/see-non-printable-characters)

[Contact me](contact) if you know of any other characters or programs that should be listed here.

This test is also available [as a plain .txt file](https://gitlab.com/nervuri/nervuri.net/-/raw/master/gopher/zero-width.txt).
