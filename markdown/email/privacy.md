``` metadata
title: Notes on e-mail privacy
description: This article delves into e-mail privacy problems and proposes solutions focused on minimizing reliance on the e-mail service provider.
table_of_contents: true
published: 2023-08-11
updated: 2023-11-01
also_on: gemini
license: CC-BY-SA 4.0
```

## Introduction

E-mail privacy is notoriously bad:

* Most people use parasitic e-mail providers with anti-privacy business models.
* Most people accumulate decades' worth of messages server-side.
* Many e-mail servers expose the sender's IP address within a message header (because the SMTP specification says they "MUST").
* TLS is optional for server-to-server connections.  Even when used, it is most often unauthenticated.  Most servers don't support DANE or MTA-STS, but do support deprecated TLS versions and weak cipher suites.
* End-to-end encryption is generally a pain to use, so hardly anyone does.
* End-to-end encryption only covers the message body, not the subject nor any of the other headers.
* E-mail clients add extraneous headers which expose information about the user.
* HTML e-mail is a thing, therefore tracking e-mail views via HTML is also a thing.
* URLs with unique identifiers are widely used for tracking.
* Et cetera.

What bothers me most is that IMAP and webmail have lead to billions of people and organizations accumulating years and years' worth of private messages on mail servers.  This increased their risk of data compromise to the point of near-certainty.  Webmail in particular made people dependent on their online accounts for accessing their e-mail archives.  And providers make zero effort to persuade people to move their data offline, even though this could be done with a couple of clicks in the webmail interface.

Given this state of affairs, it's worth figuring out how to minimize our information exposure.  On that note, here are a few features of my e-mail setup:

* E-mail is downloaded over POP3, then deleted from the server immediately.
* Connections to the server are made over TLS 1.3 only (no STARTTLS).
* All TLS connections are authenticated via certificate pinning.
* All connections are made over Tor.
* E-mail filtering is done client-side, no filtering information is stored on the server.
* Messages are rendered as plain text by default, but if HTML is required, all HTML (anti-)features that can be used for tracking are disabled.
* When sending e-mail, my timezone is spoofed.  The e-mail client does not add a User-Agent header to the message, nor any other privacy-sensitive headers like Content-Language.

This is desiderata I have irrespective of which e-mail service I'm using.  Filtering, storing and synchronizing e-mail across multiple devices should be the client's responsibility, while the server should know and do as little as possible (namely: receive messages, encrypt them to the user's public key -- ideally -- and store them until fetched).

Hardly anyone uses POP anymore, but returning to POP's download-and-delete approach would, in principle, be appropriate for most use cases, as software for end-to-end encrypted file synchronization can supplant IMAP's sync functionality -- and could also synchronize e-mail filters and various client settings.  Current mail clients don't make this approach easy in practice, but it's worth striving towards (by client developers in particular), as it's fundamentally more secure and private than the dominant server-centric paradigm.

## POP3 considerations

When using POP, there are a few things to keep in mind.  Most importantly: POP3 only downloads messages from the Inbox folder, so any server-side filters for putting specific messages in specific folders will have to be moved client-side.  Filters and folder names can contain sensitive information, so moving them off the server is a good idea anyway.  Client-side filters can either be configured in the e-mail client itself, or in a separate program like fdm.

However, there is one filter you might want to add server-side: "save all mail to inbox" (including spam).  Otherwise your client will not download messages marked as spam and you won't get a chance to see the wrongly-flagged ones.  Spam filtering can be configured client-side as well, either by making use of headers added by the server's spam checker, or by running a spam checker yourself.  Thunderbird, for example, comes with a built-in junk classifier and ClawsMail can integrate with SpamAssasin and Bogofilter via plugins.

Another issue is that e-mail service providers sometimes don't allow POP3 to work as it's supposed to.  For example, when using Microsoft's e-mail service (Outlook/Hotmail), you need to explicitly allow the POP3 client to delete messages (Settings -> Mail -> Sync email -> POP options), otherwise they'll just be moved to another folder.  In GMail, POP3 has to be explicitly enabled:

[Read Gmail messages on other email clients using POP](https://support.google.com/mail/answer/7104828?hl=en)

Some privacy-focused providers like Proton Mail and Tutanota require special client software and do not support POP3.  There is bridging software available for using Proton Mail with regular IMAP and SMTP, but there's currently no such software for Tutanota.

[hydroxide, third-party Proton Mail bridge](https://github.com/emersion/hydroxide)  
[official Proton Mail bridge](https://github.com/ProtonMail/proton-bridge)

## POP-like IMAP

IMAP can be used in a POP-like fashion as well, without being limited to the Inbox folder, but most clients don't support this mode of operation.  I've only seen this feature in getmail, which has several `delete_*` options to control the removal of messages from the server after they're fetched.  Deletion does not have to be immediate; getmail can be set to remove messages a specific number of days after they are downloaded, which means that you can still access recent messages on other computers via webmail or IMAP, and if the server gets compromised, the attacker won't get most of your mail.

[getmail 6](https://getmail6.org/configuration.html#conf-options)

When moving your e-mail archive offline, you'll need to use IMAP in order to preserve the folder structure.  Beyond that, however, bear in mind that POP3 is much simpler, has a smaller attack surface, and works in fetch-and-delete manner with every e-mail client that supports it.

## Caveats

Bear in mind:

* There is no guarantee that e-mail providers don't keep copies of deleted messages.  Even if nothing shady is going on, it is likely that e-mail is kept in server backups for a period of time, and the same goes for message metadata in server logs.  The specific period ought to be disclosed in each service provider's privacy policy.
* Mainstream providers like Google and Microsoft may not allow connections over Tor and may even block VPNs, so you might have to reveal your real IP address to them.  If you do, make sure to check whether they expose it in mail headers.

## STARTTLS vs implicit TLS

It's official: STARTTLS is to be avoided.  Appendix A of RFC 8314 details why, after a long period in which implicit TLS for e-mail protocols was deemed deprecated, it is now the recommended option.  In their 2021 Usenix paper on the topic, Damian Poddebniak et al. provided a jaw-dropping illustration of how prone STARTTLS is to implementation flaws.

* [RFC 8314 -- Cleartext Considered Obsolete: Use of Transport Layer Security (TLS) for Email Submission and Access (2018)](https://datatracker.ietf.org/doc/html/rfc8314)
* [Why TLS is better without STARTTLS: A Security Analysis of STARTTLS in the Email Context (2021)](https://www.usenix.org/conference/usenixsecurity21/presentation/poddebniak)

In short, use:

* SMTP over port 465 rather than 587
* POP3 over port 995 rather than 110
* IMAP over port 993 rather than 143

## Synchronization and backup

For synchronizing e-mail messages (and any other files) between devices in real time, there are several privacy-preserving solutions.  Proprietary programs aside, the easiest option might be Nextcloud with end-to-end encryption enabled.  You don't need to self-host, there are many Nextcloud providers, some of which offer a few GB of storage at no cost.

* [Nextcloud end-to-end encryption](https://nextcloud.com/endtoend/)
* [Nextcloud providers](https://nextcloud.com/sign-up/)

Then there's Syncthing, an excellent program for peer-to-peer sync:

* [Syncthing](https://syncthing.net/)

The problem with peer-to-peer sync is that the devices you're synchronizing need to be online at the same time.  You can work around this by also synchronizing to an "always-online" device (a server, a phone, etc).  If you don't fully trust this device, you can hide your files from it via some form of transparent file encryption.  Syncthing supports this via its "untrusted devices" feature, which is still in beta phase, but has been around for a while and reportedly works well.  I used eCryptfs instead; it is Linux-only, but there are cross-platform solutions as well, such as EncFS, securefs, CryFS and Cryptomator.

* [Syncthing -- Untrusted (Encrypted) Devices](https://docs.syncthing.net/users/untrusted.html)
* [Comparison table: transparent file encryption software](https://nuetzlich.net/gocryptfs/comparison/)
* [SiriKali's list of encryption backends](https://mhogomchungu.github.io/sirikali/)

My setup involved a phone and two computers synchronizing an encrypted folder via Syncthing; the folder was opaque to my phone, but the computers mounted it automatically on login, and synchronization was seamless.  I took these notes when setting up eCryptfs:

``` shell
# basic setup of ~/Private/ (and ~/.Private/), auto-mounted on login:
sudo apt install ecryptfs-utils
sudo modprobe ecryptfs
ecryptfs-setup-private

# setup of ~/Private/ (and ~/.Private/) WITHOUT auto-mount on login:
ecryptfs-setup-private --nopwcheck --noautomount --wrapping
ecryptfs-mount-private
ecryptfs-umount-private

# to change the passphrase:
ecryptfs-rewrap-passphrase ~/.ecryptfs/wrapped-passphrase
```

* [eCryptfs page on the Arch Linux wiki](https://wiki.archlinux.org/index.php/ECryptfs)

At the moment I don't need real-time synchronization between devices.  I rely on my backup if I have to move to a different machine.

When it comes to backup, I've used Borg and Duplicati and have heard good things about restic.  All three support encryption, so backups can be safely stored on other people's machines.  Duplicati is the more beginner-friendly one, it has a graphical interface and works on Windows.

* [Borg](https://www.borgbackup.org/)
* [restic](https://restic.net/)
* [Duplicati](https://www.duplicati.com/)

Borg is easy to set up and has great documentation.  This is how I use it to back up important files to my Raspberry Pi via SSH:

``` shell
export BORG_REPO=myraspberrypi:/home/backup/mystuff
BORG_PASSPHRASE="$(pass show bkp)"
export BORG_PASSPHRASE

borg create --progress --stats ::"{now:%Y-%m-%d}" ~/Documents ~/Mail
borg prune --verbose --list --keep-daily=7 --keep-weekly=4
```

The Pi then rsyncs the backup to an off-site server.

## Contacts and calendars

Vdirsyncer and DecSync CC can be used for synchronizing calendars and contacts as files.  Alternatively, EteSync can be used for end-to-end encrypted and history-preserving synchronization of contacts and calendars.

Vdirsyncer syncs between a CardDAV/CalDAV server and the local filesystem; it runs on GNU/Linux and macOS.  DecSync CC is an Android contacts & calendar provider which uses a local directory instead of a server; the directory can be synchronized using a separate program (see above).  EteSync is a cross-platform server-based solution with end-to-end encryption, which also saves the history of changes made to your calendar and contacts.

* [vdirsyncer](https://vdirsyncer.pimutils.org/en/stable/)
* [DecSync CC](https://github.com/39aldo39/DecSyncCC)
* [EteSync](https://www.etesync.com/)

I, for one, back up my contacts from my phone to my laptop manually.  As for the calendar, I only access it on my laptop.  Real-time synchronization is overkill for my needs.

## E-mail headers

Here are a few examples of client-added e-mail headers and what information they expose:

* User-Agent: reveals the mail client's name and version; may also include the operating system's name and version.
* X-Mailer: client name and version.
* Content-Language: languages your client is configured to use (for spellchecking and such).
* Date: system time, including timezone.
* Message-ID: may reveal the local machine's hostname and/or system time.  The algorithm for generating message IDs varies from client to client, so it can also be used to deduce what mail program you use (Thunderbird uses UUIDs, Mutt uses a Unix timestamp concatenated with a random number, etc).
* Content-Type: reveals whether or not you use format=flowed, which may stand out if it's not your client's default setting.

The order of headers can also be used for fingerprinting, as well as differences in how multi-line headers are indented.  There may also be other subtle differences between how the various e-mail clients generate headers.

## E-mail clients

### Mutt, mpop, msmtp and getmail

My current e-mail setup consists of mutt, mpop, msmtp and a small bespoke filtering script that took the place of fdm.  When I need IMAP, I use getmail.

* [mutt](http://www.mutt.org/)
* [mpop](https://marlam.de/mpop/)
* [msmtp](https://marlam.de/msmtp/)
* [getmail 6](https://getmail6.org/)
* [my configs and filtering script](https://tildegit.org/nervuri/email-configs)

I configured mpop and msmtp to retrieve mail account passwords from pass.  You could also use a variant of pass called passage that uses age instead of GPG.

* [pass](https://www.passwordstore.org/)
* [passage](https://github.com/FiloSottile/passage)

### Thunderbird

A more approachable solution for non-techies is to use Thunderbird.  The procedure for moving e-mail offline is simple: add your account via IMAP and then drag each folder to the Local Folders section.  After the transfer completes, double-check that the number of messages in your local folders is the same as on the IMAP side, then delete all IMAP folder contents.  Messages will be stored in your Thunderbird profile directory, which you can then back up:

[How to back up your Thunderbird profile directory](https://github.com/HorlogeSkynet/thunderbird-user.js/wiki/1.2-Backing-Up)

Before adding any accounts to Thunderbird, you may want to set the following in Settings -> General -> Config Editor (at the bottom):

``` Thunderbird_settings
# check all folders for new messages (including spam and trash)
mail.server.default.check_all_folders_for_new = true

# change default sort order (new messages first)
mailnews.default_sort_order = 2
mailnews.default_news_sort_order = 2
```

Thunderbird does not have good privacy protections by default, but it can be configured to get you most of the way there.  At a minimum, I would set the following:

``` Thunderbird_settings
# hide timezone
mail.sanitize_date_header = true

# don't add Content-Language header
mail.suppress_content_language = true

# don't add User-Agent header
general.useragent.override = ""
```

You can also go to "Settings -> General -> Connection" and configure Thunderbird to proxy traffic through Tor.  That's the equivalent of:

``` Thunderbird_settings
network.proxy.socks = 127.0.0.1
network.proxy.socks_port = 9050
network.proxy.socks_remote_dns = true
```

For more in-depth Thunderbird hardening, HorlogeSkynet's user.js file is worth looking into:

[Thunderbird user.js](https://github.com/HorlogeSkynet/thunderbird-user.js)

This is not to say I recommend it in its entirety.  For instance, it sets `privacy.resistFingerprinting = true`, which paradoxically makes Thunderbird more fingerprintable by adding a Firefox-specific User-Agent header to all outgoing e-mail.  This setting was made for Firefox and does not work well with Thunderbird.

See also:

[How do I disable telemetry in Thunderbird?](https://superuser.com/questions/1672309/how-do-i-disable-telemetry-in-thunderbird)

## Throwaway addresses, aliases, catch-all

The e-mail address is an identifier that links one's online accounts.  I generally don't want them linked, so most often I don't reveal my real e-mail address to services I sign up to.

When I need a throwaway address for an unimportant account, I reach for anonbox, Guerilla Mail or (for longer-term addresses) danwin1210.de and Fedora Email (not affiliated with the distro).  I use SimpleLogin when I want messages to be forwarded to one of my real addresses.  In cases where I don't want messages going through a third-party e-mail forwarder, I fall back to catch-all e-mail on my own domain.

* [anonbox](https://anonbox.net/)
* [Guerilla Mail](https://www.guerrillamail.com/)
* [danwin1210.de](https://danwin1210.de/mail/)
* [Fedora Email](https://fedora.email/)
* [SimpleLogin](https://simplelogin.io/)

If you start receiving spam, using a unique address for each service allows you to identify which one leaked your address to the spammers.  It also enables you to block addresses that are receiving unwanted messages.

## Tracking links

Messages sent by companies very often include links with tracking parameters.  Often these parameters can be stripped without breaking the link, which is something that mail clients can do automatically, to some extent.  For instance, Proton Mail has this feature:

[Proton Mail tracking links protection](https://proton.me/blog/tracking-links-protection)

Thunderbird inherits a similar feature from Firefox:

[Firefox Query Parameter Stripping](https://firefox-source-docs.mozilla.org/toolkit/components/antitracking/anti-tracking/query-stripping/index.html)

```
privacy.query_stripping.enabled = true
privacy.query_stripping.enabled.pbmode = true
```

Proton Mail also prompts users when they click links, displaying the links in full and asking for confirmation before opening them.  This is meant to protect against phishing and could be enhanced by presenting the link in way that's easier to read, highlighting the hostname.

The more mail clients incorporate such features, the better.

[Tracking link test page](https://fmarier.github.io/brave-testing/query-filter.html)

## Attach?

Sometimes institutions and companies request sensitive information via e-mail.  You may be asked to attach a photo of your ID, for example.  Consider that, no matter what their privacy policies say, the number of organizations with good e-mail hygiene is close to zero.  Messages remain on their servers (probably owned and managed by third parties) for who knows how long, increasing the odds that your data will be compromised.  So, if possible, send an ephemeral link to the document instead.  Either link to the file on a server you control or use a service which supports in-browser encryption.  I have used:

* [send.vis.ee (fork of Firefox Send)](https://send.vis.ee/)
* [Lufi -- Disroot file uploader](https://upload.disroot.org/)
* [send.tresorit.com](https://send.tresorit.com/)
* [mega.io](https://mega.io/)

Some employees are trained never to click links in e-mail, so this may not always work, but it's worth trying.  Also consider that it may be more private to present the information in person or send it via postal mail.  Paper is better for privacy; digital copies tend to multiply.

## End-to-end encryption

It's strange to me that so many e-mail privacy guides jump straight to PGP when there are much lower-hanging fruit that are more beneficial to most people's privacy.  Still, end-to-end encryption (via PGP, S/MIME, codecrypt, etc) is important in some contexts and should be used when appropriate.

There have been developments on the PGP UX front.  For one thing, Proton Mail made PGP work such that users don't need to think about it or even know what it is.  Proton Mail supports PGP encryption to external servers as well, with key discovery done automatically via WKD.  The Pretty Easy Privacy project has also made noteworthy advances on the PGP UX front, as has Mailvelope.  Researchers may continue to publish papers about how Johnny can't encrypt, but the tools have clearly gotten better and can get better still.

* [Proton Mail supports PGP WKD](https://proton.me/blog/security-updates-2019)
* [p≡p (pretty Easy privacy) Foundation](https://pep.foundation/)
* [Mailvelope](https://mailvelope.com/)
* [Why Johnny Still, Still Can’t Encrypt](https://arxiv.org/pdf/1510.08555.pdf)

Modern keyservers like keys.openpgp.org send verification e-mails to check if the key owner really controls the e-mail address(es) associated with the key.  Going further, projects like Keyoxide and Keybase offer proofs of online identity by associating public accounts and website(s) with PGP keys in a verifiable way.  This helps greatly when assessing whether a particular key belongs to who it's supposed to.

* [keys.openpgp.org](https://keys.openpgp.org/)
* [Keyoxide](https://keyoxide.org/)
* [Keybase](https://keybase.io/)

The Memory Hole project was a standardization effort that addressed PGP encryption and/or signing of e-mail headers.  The proposed standard was implemented by programs such as Enigmail and Mailpile.  The most recent push in this regard is the IETF draft "Protected Headers for Cryptographic E-mail", the last revision of which was published in December 2019.  Mutt, for example, has implemented coverage for the Subject header (see the `crypt_protected_headers_read` option).

* [Memory Hole](https://modernpgp.org/memoryhole/)
* [Protected Headers for Cryptographic E-mail](https://datatracker.ietf.org/doc/draft-autocrypt-lamps-protected-headers/)

There is also a standard called Autocrypt for negotiating end-to-end encryption between two users of e-mail:

* [Autocrypt](https://autocrypt.org/)

Lastly, there is software for e-mail servers which automatically encrypts incoming mail to the recipient's PGP key.  This is not end-to-end encryption, but it's noteworthy nonetheless.

* [Lacre](https://git.disroot.org/Disroot/gpg-lacre)

Further reading/viewing:

* [Jens Müller -- Covert Attacks on Email End-to-End Encryption](https://onion.tube/watch?v=2vFMSJLaOOQ)
* [EFAIL](https://efail.de/)
* [Neal H. Walfield -- Why I’m not giving up on PGP](https://arstechnica.com/information-technology/2016/12/signal-does-not-replace-pgp/)
* [Forward Secrecy Extensions for OpenPGP](https://datatracker.ietf.org/doc/html/draft-brown-pgp-pfs-03)

## Closing thoughts

What I'm ultimately hoping for is the development of a newbie-friendly private-by-default e-mail client which nails it on every privacy front.  One such effort worth keeping an eye on is Mailpile, which is currently being overhauled with help from NLnet.

[Mailpile 2 -- "A Mail Client in Six Steps"](https://www.mailpile.is/blog/2023-05-01_A_Mail_Client_in_Six_Steps.html)

I'd also like to see better integration between server and client, such that the client is able to:

* show the mail service provider (MX records) for each destination address;
* show if the server is going to use TLS for the connection to the destination server and what kind of authentication (if any) it will perform;
* set per-domain TLS policies (see Postfix's `smtp_tls_policy_maps`);
* inform the user when a message has been deferred (left in the server queue), what the reason was, when the retries are scheduled to occur, and notify the user when it gets delivered (in case of greylisting, for instance).

That's about it.

P.S.
[Use plain text email.](https://useplaintext.email/)

---

## Contributions

Thanks to sl1200 for letting me know about Syncthing's "untrusted devices" feature and reminding me of Keyoxide.

[sl1200](https://dystopic.world/)
