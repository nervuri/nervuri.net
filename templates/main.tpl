<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<link rel="icon" type="image/ico" href="${PATH}favicon.ico"/>
<meta name="theme-color" content="#000"/>
<meta name="color-scheme" content="dark"/>
<meta name="referrer" content="no-referrer"/>
<title>$TITLE</title>
$EXTRA_TAGS
<link rel="alternate" type="application/atom+xml" title="nervuri Atom feed" href="${PATH}feed.atom"/>

<link rel="pgpkey authn" href="${PATH}keys/nervuri-pgp.asc"/>
<link rel="webmention" href="https://webmention.io/nervuri.net/webmention"/>
<link rel="pingback" href="https://webmention.io/nervuri.net/xmlrpc"/>

<link rel="stylesheet" href="${PATH}style.css"/>
</head>
<body>
$TOP
<main>
$MAIN
</main>
$FOOTER
</body>
</html>
