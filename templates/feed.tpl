<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">

  <title>$TITLE</title>
  <subtitle>$SUBTITLE</subtitle>
  <id>$ID</id>
${LINK_ALT}  <link rel="self" href="$LINK_SELF"/>
  <icon>$ICON</icon>
  <updated>${UPDATED}T00:00:00Z</updated>
  <author>
    <name>$AUTHOR</name>
  </author>
  <rights>$RIGHTS</rights>
$ENTRIES
</feed>
