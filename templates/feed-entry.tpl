
  <entry>
    <title>$TITLE</title>
    <id>$ID</id>
${LINK_ALT}    <published>${PUBLISHED}T00:00:00Z</published>
    <updated>${UPDATED}T00:00:00Z</updated>
    <summary>$SUMMARY</summary>
    <content type="html"><![CDATA[$CONTENT]]></content>
    <rights>$RIGHTS</rights>
  </entry>
