web:
	./gen.py

check:
	find public/ -name '*.atom' -exec xmllint --noout {} \;
	find public/ -name '*.html' ! -path 'public/zero-width.html' -exec xmllint --noout {} \;
	find public/ -name '*.html' -exec sh -c 'tidy -q -errors -access "{}" || ls "{}"' \;

antenna:
	torsocks agunua "gemini://warmedal.se/~antenna/submit?gemini%3A%2F%2Frawtext.club%2F~nervuri%2F"

.PHONY: web check
