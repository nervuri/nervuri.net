# Acceptable space characters

=> https://gitlab.com/gemini-specification/gemini-text/-/issues/15

The specification defines whitespace as "any non-zero number of consecutive spaces or tabs".  Unicode contains many types of spaces.  Are they all acceptable for use in, for instance, link lines?

Also, list items are defined as lines beginning with "* ".  That is an asterisk followed by the usual U+0020 space character.  Should it be "*<whitespace>", for consistency?

Until the specification clears these things up, I would advise a conservative implementation - only allow:
* U+0020 (space) and U+0009 (tab) for link lines
* U+0020 (space) for list items

## Links

These links should work:
=> gemini://gemini.circumlunar.space/ Project Gemini [U+0020 (space)]
=>	gemini://gemini.circumlunar.space/	Project Gemini [U+0009 (tab)]
=> 	 	gemini://gemini.circumlunar.space/ 	 	Project Gemini [mix of spaces and tabs]

These links should not work [U+2003 (em space) is used]:
=> gemini://gemini.circumlunar.space/
=>  gemini://gemini.circumlunar.space/
=>   gemini://gemini.circumlunar.space/
=>gemini://gemini.circumlunar.space/ Project Gemini

## Lists

* Valid list item [space]
*	Invalid list item [tab]
* Invalid list item [U+2003 (em space)]
*Invalid list item [no whitespace]
