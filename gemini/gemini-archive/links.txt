# mailing list archive (down)
https://lists.orbitalfox.eu/archives/gemini/

# IRC logs for the #gemini channel on https://tilde.chat/
gemini://tilde.team/~tomasino/irc/log.txt
gemini://makeworld.space/irc/

# git repos
git://gemini.circumlunar.space/gemini-site
https://gitlab.com/gemini-specification/protocol.git
https://gitlab.com/gemini-specification/gemini-text.git
